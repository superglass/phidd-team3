package edu.neu.phidd.team3.myexecutive.speech;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.widget.Toast;

public class SpeechToText{
	
	private static final String TAG = SpeechToText.class.getSimpleName();
	public static final int REQUEST_CODE = 1234;
	
	Context context;
	Activity activity;
	
	public SpeechToText(Context context){
		this.context = context;
		this.activity = (Activity) context;
	}
	
	public boolean startSpeechRecognition(){
		// Check to see if speech recognition is available
		if(! speechAvailabilityTest()){
			Toast.makeText(context, "Voice recognition is not available on your device", Toast.LENGTH_LONG).show();
			return false;
		}
		
		if(isConnected()){
			Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
	        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
	        RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
	        activity.startActivityForResult(intent, REQUEST_CODE);
	        
	        return true;
		}
	    else{
	    	Toast.makeText(context, "Please connect to the internet and try again.", Toast.LENGTH_LONG).show();
	    	return false;
	    }
	}
	
	public String getSpeechToTextResult(Intent data){
		ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
		if(matches.size() == 0){
			return null;
		}
		return matches.get(0);
	}
	
	// Method to check if speech recogniton apps are available on the device
	private boolean speechAvailabilityTest(){
		PackageManager pm = context.getPackageManager();
		List activities = pm.queryIntentActivities(new Intent( RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);
		if (activities.size() == 0) {
			Log.d(TAG, "Voice recognition is not available!");
			return false;
		}
		Log.d(TAG, "Voice recognition is available");
		return true;
	}
	
	// Check internet connection
	private  boolean isConnected(){
	    ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo net = cm.getActiveNetworkInfo();
	    if (net!=null && net.isAvailable() && net.isConnected()) {
	    	Log.d(TAG, "Internet connection is available");
	        return true;
	    } 
	    Log.d(TAG, "Internet connection is not available!");
	    return false;
	}
}