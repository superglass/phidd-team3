package edu.neu.phidd.team3.myexecutive;

import java.io.File;
import com.google.android.glass.widget.CardBuilder;
import edu.neu.phidd.team3.myexecutive.audio.PlayAudio;
import edu.neu.phidd.team3.myexecutive.audio.RecordAudio;
import edu.neu.phidd.team3.myexecutive.task.TaskManager;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.WindowManager;

public class AudioRecordActivity extends Activity {
	
	private static final String TAG = AudioRecordActivity.class.getSimpleName();

	TaskManager mTaskManager;
	RecordAudio mRecordAudio;
	PlayAudio mPlayAudio = null;
	CardBuilder mainCard;
	boolean recordingStarted = false;
	String audioPath;

	@Override
	protected void onStop() {

		super.onStop();
		
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		super.onCreate(savedInstanceState);
		mTaskManager = TaskManager.getInstance();
		mRecordAudio = RecordAudio.getInstance();
		mainCard = new CardBuilder(this, CardBuilder.Layout.TEXT)
					.setText("Record your instructions");
		setContentView(mainCard.getView());
		
		// Record with MediaRecorder
		//startRecording();
	}
	
	@Override
	protected void onResume() {

		super.onResume();
		mainCard.setFootnote("Recording...");
		setContentView(mainCard.getView());
		startRecording();
		new MonitorAmplitudeTask().execute();
		
	}

	private void startRecording(){
		File currentTaskFile = mTaskManager.getCurrentTaskInfo().getTaskFolder();
		
		String path = currentTaskFile.getAbsolutePath();
		Log.d(TAG, "Current task path: " + path);

		// Set the path for the audio file
		int currentStep = mTaskManager.getCurrentStep();
		
		audioPath = path + "/Recording" + currentStep + ".mp4";
		Intent result = new Intent();
		result.putExtra("audioFilePath", audioPath);
		setResult(RESULT_OK, result);
		mRecordAudio.setAudioFileName(audioPath);
		mRecordAudio.prepare();
		mRecordAudio.startRecording();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
//		if(keyCode == KeyEvent.KEYCODE_DPAD_CENTER){
//			if(!recordingStarted){
//				mainCard.setText("Recording...");
//				mainCard.setFootnote("Tap again to stop recording");
//				setContentView(mainCard.getView());
//				startRecording();
//				recordingStarted = true;
//				new MonitorAmplitudeTask().execute();
//			} else {
//				mRecordAudio.stopRecording();
//				
//				mainCard.setText("Recording complete.");
//				setContentView(mainCard.getView());
//				if(mPlayAudio != null)
//					mPlayAudio.releasePlayer();
//				finish();
//			}
//			return true;
//		}
		if(keyCode == KeyEvent.KEYCODE_BACK){
			
			finish();
		}
		return false;
	}
		
	class MonitorAmplitudeTask extends AsyncTask<Void, Double, Void> {
		boolean endOfRecording = false;
		int silientTimes = 0;
		final int TIMES_TO_STOP = 20;
		final int threshold = 1600;
		
		@Override
		protected Void doInBackground(Void... params) {
			while(!endOfRecording){
				int amplitude = mRecordAudio.getMaxAmplitude();
				if(amplitude<=threshold){
					silientTimes++;
				} else {
					silientTimes=0;
				}
				if(silientTimes>=TIMES_TO_STOP){
					endOfRecording =true;
				}
				Log.d("Amplitude", "amplitude:"+amplitude);
				
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			Log.d("Amplitude", "Finish!");
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			mRecordAudio.stopRecording();
			
			mainCard.setFootnote("Recording complete.");
			setContentView(mainCard.getView());
			if(mPlayAudio != null)
				mPlayAudio.releasePlayer();
			finish();
			super.onPostExecute(result);
		}	
	}
}
