package edu.neu.phidd.team3.myexecutive;


import com.google.android.gcm.GCMBaseIntentService;
import com.google.android.glass.media.Sounds;

import edu.neu.phidd.team3.myexecutive.audio.InstructionManager;
import edu.neu.phidd.team3.myexecutive.gcm.GCMHelper;
import edu.neu.phidd.team3.myexecutive.helper.TaskActivity;
import edu.neu.phidd.team3.myexecutive.livecard.LiveCardService;
import edu.neu.phidd.team3.myexecutive.videocall.AudioCall;
import edu.neu.phidd.team3.myexecutive.videocall.AudioCallUtil;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.preference.PreferenceManager;
import android.provider.MediaStore.Audio;
import android.util.Log;
import android.view.SoundEffectConstants;
import android.widget.Toast;

public class GCMIntentService extends GCMBaseIntentService {
	private static final String PROJECT_NUMBER = "134717269089";
    private static final String TAG = GCMIntentService.class.getSimpleName(); //"GCM Test";
	public GCMIntentService() {
        super(PROJECT_NUMBER);
        Log.d(TAG, "GCMIntentService init");
    }

	@Override
	protected void onError(Context ctx, String sError) {
		// TODO Auto-generated method stub
		Log.d(TAG, "Error: " + sError);
		
	}
	
	/**
	 * Do what you want with the intent.
	 * To pull the message, use the {@link edu.neu.phidd.team3.myexecutive.gcm.GCMHelper#EXTRA_MESSAGE}
	 * 
	 * @author joe
	 * @param Context
	 * @param intent
	 * */
	@Override
	protected void onMessage(Context ctx, Intent intent) {
		AudioManager am = (AudioManager) ctx.getSystemService(AUDIO_SERVICE);
		if(AudioCallUtil.callInProgress){
			return;
		}
		if(intent.getStringExtra(GCMHelper.EXTRA_MESSAGE).equals(GCMHelper.VIDEO_CHAT)){
			// The message is from the helper to the user with the audio chat session id
			// Extract the audio chat session id
			String chatSessionID = intent.getStringExtra(GCMHelper.VIDEO_CHAT_SESSION_ID);
			
			// Start the chat session at the user end
//        	Intent i = new Intent(this, AudioCall.class);
//        	Log.d(TAG, "Session ID at the user side: " + chatSessionID);
//        	i.putExtra(GCMHelper.VIDEO_CHAT_SESSION_ID, chatSessionID);
//        	i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//	    	startActivity(i);
	    	
	    	// Send a broadcast
	    	sendBroadcast(new Intent(GCMHelper.VIDEO_CHAT).putExtras(intent.getExtras()));
	    	
		}
		else if(intent.getStringExtra(GCMHelper.EXTRA_MESSAGE).equals(GCMHelper.TUTORIAL_STARTED)){
			// Message from a user device to a helper device
			// notifying the helper that the user has started using the tutorial
			// Ask the helper if he wants to help the user in this session
			// Start the live card at the helper device
			stopService(new Intent(ctx, LiveCardService.class));
			startService(new Intent(ctx, LiveCardService.class).putExtras(intent.getExtras()));
			am.playSoundEffect(Sounds.SUCCESS);
		}
		else if(intent.getStringExtra(GCMHelper.EXTRA_MESSAGE).equals(GCMHelper.PERMISSION_GRANTED)){
			// The author has granted permission to help during this session
			// Set the helpSetting flag in the user's task activity
			TaskActivity.helpSetting = true;
		}
		else if(intent.getStringExtra(GCMHelper.EXTRA_MESSAGE).equals(GCMHelper.HELP_DENIED)){
			// The author has denied permission to help
			// Set the helpSetting flag in the user task activity to false
			TaskActivity.helpSetting = false;
		}
		else if(intent.getStringExtra(GCMHelper.EXTRA_MESSAGE).equals(GCMHelper.NOT_NOW)){
			// Send a broadcast to the user TaskActivity
			sendBroadcast(new Intent(GCMHelper.NOT_NOW));
		}
		else if(intent.getStringExtra(GCMHelper.EXTRA_MESSAGE).equals(GCMHelper.TIME_UPDATE)){
			// An update from the user to the helper on the amount of time spent in the current step
			// Update the live card at the helper end
			// Start the live card at the helper device
			stopService(new Intent(ctx, LiveCardService.class));
			startService(new Intent(ctx, LiveCardService.class).putExtras(intent.getExtras()));
			am.playSoundEffect(Sounds.SUCCESS);
		}
		else if(intent.getStringExtra(GCMHelper.EXTRA_MESSAGE).equals(GCMHelper.QUICK_HELP)){
			// The message is from the user to the helper seeking help
			// Extract the user's device ID and the extra message
			String user_deviceID = intent.getStringExtra(GCMHelper.USER_DEVICE_ID);
			String user_message = intent.getStringExtra(GCMHelper.EXTRA_MESSAGE);
			String learner_regid = intent.getStringExtra(GCMHelper.EXTRA_LEARNER_REGID);
			Log.d("GCM Test", "GCMIntentService Learner regid is :"+learner_regid);
			// Start the live card at the helper device
			Log.d(TAG,"Start service from receiver.");
			stopService(new Intent(ctx, LiveCardService.class));
			startService(new Intent(ctx, LiveCardService.class).putExtras(intent.getExtras()));
			am.playSoundEffect(Sounds.SUCCESS);
		}		
	}

	 @Override
	 public android.os.IBinder onBind(Intent intent) {
		return null;

	 };
		
	
	@Override
	protected void onRegistered(Context ctx, String regId) {
		// TODO Auto-generated method stub
		//We are not sending registrationID to the server, but storing it using shared preference
		Log.d(TAG, regId);
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString("registrationIdOfSender",regId);
		editor.commit();
		Log.i(TAG, "Registered with registration id "+regId);
		
		
	}

	@Override
	protected void onUnregistered(Context ctx, String regId) {
		// TODO Auto-generated method stub
		// send notification to your server to remove that regId
		
	}
}
