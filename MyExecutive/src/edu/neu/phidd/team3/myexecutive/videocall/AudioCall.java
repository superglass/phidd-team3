package edu.neu.phidd.team3.myexecutive.videocall;

import com.google.android.glass.view.WindowUtils;
import com.google.android.glass.widget.CardBuilder;

import edu.neu.phidd.team3.myexecutive.R;
import edu.neu.phidd.team3.myexecutive.gcm.GCMHelper;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

public class AudioCall extends Activity {
	
	private static final String TAG = AudioCall.class.getSimpleName();
	
	String chatSessionID;
	AudioCallUtil mAudioCallUtil;
	CardBuilder mainCard;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		getWindow().requestFeature(WindowUtils.FEATURE_VOICE_COMMANDS);
		mainCard = new CardBuilder(this, CardBuilder.Layout.CAPTION).setText("Audio chat");
		setContentView(mainCard.getView());
		
		// Get the Audio chat util instance
		mAudioCallUtil = AudioCallUtil.getInstance();
		
		// Get the chat session ID
		Intent intent = getIntent();
		chatSessionID = intent.getStringExtra(GCMHelper.VIDEO_CHAT_SESSION_ID);
		
		// Start audio call
		mAudioCallUtil.startAudioCall(this, chatSessionID);		
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.clear();
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.video_call, menu);
		return true;
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		if(keyCode == KeyEvent.KEYCODE_DPAD_CENTER){
			openOptionsMenu();
			return true;
		}
		if(keyCode == KeyEvent.KEYCODE_BACK){
			
			// End the audio call
			mAudioCallUtil.endAudioCall(this);
			
			// Finish the activity
			finish();
		}
		return false;
	}
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		
		Log.d(TAG, "Id of the selected menu item: " + id);
		if (featureId == WindowUtils.FEATURE_VOICE_COMMANDS) {
			Log.d(TAG, "Using Voice Command");
		}
		
		if(id == R.id.action_stop_video_call){
			// End the audio call
			mAudioCallUtil.endAudioCall(this);
			
			// Close the audio chat activity
			finish();
		}	
		return super.onMenuItemSelected(featureId, item);
	}
	
	@Override
	protected void onPause(){
		// End the audio call
		mAudioCallUtil.endAudioCall(this);
							
		// Close the audio chat activity
		finish();
		
		super.onPause();
	}
	
	@Override
	protected void onDestroy(){
		// Unregister the call
		mAudioCallUtil.unRegisterCall();
		
		super.onDestroy();
	}

	@Override
	public boolean onCreatePanelMenu(int featureId, Menu menu) {
		return true;
	}

	
	@Override
	public boolean onPreparePanel(int featureId, View view, Menu menu) {
		menu.clear();
        if (featureId == WindowUtils.FEATURE_VOICE_COMMANDS) {
        	// Inflate the menu; this adds items to the action bar if it is present.
    		getMenuInflater().inflate(R.menu.video_call, menu);
    		return true;
        }
        // Pass through to super to setup touch menu.
		return super.onPreparePanel(featureId, view, menu);
	}
}
