package edu.neu.phidd.team3.myexecutive.videocall;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.util.Log;
import android.widget.Toast;
import com.openclove.ovx.OVXCallListener;
import com.openclove.ovx.OVXException;
import com.openclove.ovx.OVXView;
import edu.neu.phidd.team3.myexecutive.audio.InstructionManager;

public class AudioCallUtil{
	private static final String API_KEY = "zbext6v44fdky70w4c146cin";
	
	private static AudioCallUtil instance;
	OVXView ovxView;
	InstructionManager mInstructionManager;
	Activity contextActivity;
	public static boolean callInProgress = false;
	
	private AudioCallUtil(){
		mInstructionManager = InstructionManager.getInstance();
	}
	
	public static AudioCallUtil getInstance(){
		if(instance==null){
			instance = new AudioCallUtil();
		}
		return instance;
	}
	
	public void startAudioCall(Activity activity, String chatSessionID){
		
		// Shared instance of the OVXView
		ovxView = OVXView.getOVXContext(activity);
		
		// Set all the parameters
		try {
			
			// Set the API Key
			ovxView.setApiKey(API_KEY);
			
			// Find ID of this device
			final String deviceID = BluetoothAdapter.getDefaultAdapter().getAddress();
	    	
	    	// Set OVX User ID
			Log.d("Audio Call", "User ID in AudioCall is " + deviceID);
	    	ovxView.setParameter("ovx-userId", deviceID);
	    	
	    	// Set OVX session ID
	    	Log.d("Audio Call", "Session ID in AudioCall is " + chatSessionID);
	    	ovxView.setParameter("ovx-session",chatSessionID);//Mandatory
	    	
	    	// Enable the chat
	    	ovxView.setParameter("ovx-chat", "enable");
	    	
	    	// Set auto-start to false
	    	ovxView.setParameter("ovx-autostart", "false");
	    	
	    	// Enable video chat mode
	    	ovxView.setParameter("ovx-type", "voice");
	    	
	    	// Set listeners
	    	callListener(activity);
	    	
	    	// Start the video chat
	    	if (!ovxView.isCallOn()){
	    		ovxView.call();
	    		Toast.makeText(activity, "Connecting to audio chat...", Toast.LENGTH_SHORT).show();
	    		mInstructionManager.play("Connecting to audio chat");
	    		callInProgress = true;
	    	}
	    	else{
	    		// Call is already on
	    		Toast.makeText(activity, "The audio call is already on", Toast.LENGTH_SHORT).show();
	    		mInstructionManager.play("The audio call is already on");
	    	} 	
		} catch (OVXException e) {
			e.printStackTrace();
		}		
	}
	
	public void endAudioCall(Activity activity){
		// End the audio call
		if (ovxView.isCallOn()) { 
			ovxView.exitCall(); // ends the existing call and removes the user from the live conference.
		}
	}
	
	public void unRegisterCall(){
		// Unregister the ovxView
		ovxView.unregister();
	}
	
	/* 
	 * We can listen to call related events like call started , call ended , call failed and perform appropriate actions 
	 * in these callback functions, we can also receive messages sent from the other parties in the conference using the call listener. 
	 */

	public void callListener(final Activity activity) {
		
		/** call back listener to listen to call events  */
		ovxView.setCallListener(new OVXCallListener() {
			
			//invoked when the call has been disconnected by the user.
			@Override
			public void callEnded() {
				// TODO Auto-generated method stub
				Toast.makeText(activity, "Audio call over", Toast.LENGTH_SHORT).show();
	    		mInstructionManager.play("Audio call over");
	    		callInProgress = false;
			}

			// invoked when the call fails due to some reasons.
			@Override
			public void callFailed() {
				// TODO Auto-generated method stub
				callInProgress = false;
				Toast.makeText(activity, "Not able to start the call", Toast.LENGTH_SHORT).show();
	    		mInstructionManager.play("Not able to start the call");
			}

			// invoked when the call has been established.
			@Override
			public void callStarted() {
				// TODO 
				Toast.makeText(activity, "Audio call started", Toast.LENGTH_SHORT).show();
			  	mInstructionManager.play("Audio call started");
			}

			//invoked when the call has been terminated due to n/w issues.
			@Override
			public void callTerminated(String arg0) {
				// TODO Auto-generated method stub
				Toast.makeText(activity, "Audio call over", Toast.LENGTH_SHORT).show();
	    		mInstructionManager.play("Audio call over");
	    		callInProgress = false;
			}

			@Override
			public void onNotificationEvent(String arg0, String arg1) {
				
			}

			@Override
			public void opxAuthenticationFailed(String arg0) {
				
			}

			@Override
			public void opxConnectionClosed(int arg0, String arg1) {
				
			}

			@Override
			public void opxConnectionFailed(String arg0) {
				
			}

			@Override
			public void opxConnectionReady() {
				
			}

			@Override
			public void opxDidReceiveMessage(String arg0) {
				
			}

			@Override
			public void ovxReceivedData(String arg0) {
				
			}

			@Override
			public void recordedCallStart() {
				
			}

			@Override
			public void recordedCallStop(String arg0) {
				
			}
		});
	}
}