package edu.neu.phidd.team3.myexecutive.task;

import java.io.DataInputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.os.Environment;
import android.util.Log;

/**
 * TaskManager handles all interaction with the Task.
 * 
 * To get an instance of TaskManager, call {@link #getInstance()}.
 * 
 * {@link #readTaskInfo(String)} must be called before any other operation.
 * 
 * {@link #saveTaskInfo()} will save all changes to the task
 * 
 * @author yihuaqi
 * 
 */
public class TaskManager {

	public final static int EDIT_MODE = 10000;
	public final static int VIEW_MODE = 10001;
	public final static int SEND_MODE = 10002;
	public final static int DELETE_MODE = 10003;
	public final static int RECEIVE_MODE = 10004;
	public final static int RENAME_MODE = 10005;
	public final static String TAG = "TaskManager";
	
	private int currentMode = 0;

	public void setMode(int mode) {
		Log.d(TAG, "set mode to:"+mode);
		currentMode = mode;
	}
	public TaskInfo getCurrentTaskInfo(){
		return currentTaskInfo;
	}

	public int getMode() {
		return currentMode;
	}

	/**
	 * The root folder path in which all tasks are stored
	 */
	private String tasksFolderPath = Environment.getExternalStorageDirectory()
			.getPath() + "/MyExecutive/";

	/**
	 * All task folders.
	 */
	private List<File> allTaskFolders = new ArrayList<File>();

	/**
	 * Current TaskInfo. This will be initialized only after
	 * {@link #createTaskInfo(String)} or {@link #readTaskInfo(String)}
	 */
	private TaskInfo currentTaskInfo = null;

	private static TaskManager instance;

	/**
	 * Current step in the task.
	 */
	private int currentStep = 0;

	private TaskManager() {

	}

	/**
	 * 
	 * @return an instance of the TaskManager
	 */
	static public TaskManager getInstance() {
		if (instance == null) {
			instance = new TaskManager();
		}
		return instance;
	}

	/**
	 * Set the current step.
	 * 
	 * @param step
	 */
	public void setStep(int step) {
		currentStep = step;
	}
	
	public int getCurrentStep(){
		return currentStep;
	}

	public int getTotalSteps() {
		return currentTaskInfo.getTotalSteps();
	}

	/**
	 * 
	 * @return The image file of current step
	 */
	public File getCurrentImageFile() {
		return currentTaskInfo.getImageFileByStep(currentStep);
	}

	/**
	 * 
	 * 
	 * @return The Audio file of current step
	 */
	public File getCurrentAudioFile() {
		return currentTaskInfo.getAudioFileByStep(currentStep);
	}

	/**
	 * Create a new TaskInfo.
	 * 
	 * @param taskName
	 */
	public void createTaskInfo(final String taskName) {
		File taskFolder = new File(tasksFolderPath + "/" + taskName);
		currentTaskInfo = new TaskInfo(taskFolder);
	
		setStep(0);
	}
	

	/**
	 * This method will make a new root folder if it doesn't exist.
	 * 
	 * @return a list of task folders
	 */
	public List<File> getAllTaskFolders() {
		allTaskFolders.clear();
		File tasksFolder = new File(tasksFolderPath);
		if (!tasksFolder.exists()) {
			if (tasksFolder.mkdir()) {
				Log.d(TAG, "Created directory successfully");
			} else {
				Log.d(TAG, "Directory not created");
			}
		}

		File[] tasksFiles = tasksFolder.listFiles();
		if (tasksFiles == null) {
			Log.d(TAG, "TasksFiles is null");
		} else {
			
			for (int i = 0; i < tasksFiles.length; i++) {
			
				allTaskFolders.add(tasksFiles[i]);
			}
		}

		return allTaskFolders;

	}

	/**
	 * Given the index of the file, read the TaskInfo.
	 * 
	 * @param taskName
	 * @throws Exception
	 */
	public void readTaskInfo(int index) {

		currentTaskInfo = new TaskInfo(allTaskFolders.get(index));
		setStep(0);
	}

	/**
	 * Save the TaskInfo.
	 */
	public void saveTaskInfo(String taskName) {
		currentTaskInfo.saveTaskByMeta();
		
		currentTaskInfo.renameTask(taskName);
	}
	
	/**
	 * Save the TaskInfo.
	 */
	public void saveTaskInfo() {
		Log.d("MetaInfo", "save task by meta");
		currentTaskInfo.saveTaskByMeta();
	}

	/**
	 * 
	 * @param image
	 *            The image file that is to be added
	 */
	public void setImageToCurrentStep(File image) {
		Log.d(TAG, "Inside setImageToCurrentStep");
		currentTaskInfo.setImageByStep(currentStep, currentTaskInfo.renameImageByStep(currentStep, image));
		
	}


	/**
	 * 
	 * @param audio
	 *            The audio file that is to be added
	 */
	public void setAudioToCurrentStep(File audio) {
		Log.d("MetaInfo", "Set Audio To CurrentStep :"+audio.getName());
		currentTaskInfo.setAudioByStep(currentStep, currentTaskInfo.renameAudioByStep(currentStep, audio));
	}

	public boolean createNextStep() {
		return currentTaskInfo.addStepAfterCurrent(currentStep, currentMode);
	}
	public void deleteTask() {
		currentTaskInfo.deleteTask();
		
	}
	
	public void receiveFile(DataInputStream dis){
		currentTaskInfo.receiveFile(dis);
	};
	
	public void sendTutorial(){
		currentTaskInfo.sendTutorial();
	};
	
	public String getAuthorId(){
		return currentTaskInfo.getAuthorId();
	}
}
