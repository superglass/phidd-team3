package edu.neu.phidd.team3.myexecutive.task;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import edu.neu.phidd.team3.myexecutive.bluetooth.BluetoothService;
import edu.neu.phidd.team3.myexecutive.gcm.GCMHelper;
import android.util.Log;

/**
 * This is a class that stores all information of a task, including task name, total steps, 
 * time limit for each step, all image files, all audio files, and the folder where the task is stored.
 * @author yihuaqi
 *
 */
public class TaskInfo {
	
	/**
	 * Total number of steps of this task
	 */
	int totalStep = -1;
	/**
	 * IMEI of the author glass device
	 */
	String author;
	/**
	 * All image files of this task, ordered by the step number.
	 */
	List<File> images;
	/**
	 * All audio files of this task, ordered by the step number.
	 */
	List<File> audios;
	
	
	String taskName;
	File taskFolder;
	/**
	 * MetaInfo file that stores the total steps, author of the task, and other information.
	 */
	File metaInfo;
	String metaInfoFilename = "meta";
	
	/**
	 * Given a folder of the task, construct a TaskInfo object. If the folder does not
	 * exist, it will create the folder, otherwise it will read in all meta information and
	 * all files that will be displayed in the task.
	 * @param folder
	 */
	public TaskInfo(File folder){
		 taskFolder = folder;
		 taskName = folder.getName();
		 if(taskFolder.exists()){
			 
			 //init();
			readTaskByMeta();
		 } else {
			 taskFolder.mkdirs();
			 
			 author = GCMHelper.getRegId();
			 
			 // For testing
			 //author = "APA91bEK7qsi_L_YrYsPakAZvpeN2lU3oDWHQIl78Uh4NNAKKcz6hnp_-nVDYLdUupZSYpslzvBlRltI9uUYdW7vRgr8uSHtzjdHu2wmBYoKkzeSpG_Lq5t2_qWGcLXm-RhAeXP45C2tf02XueBcsyuWpG-Ne6sxYYOnp--41ivGWlDHc4Y84Vs";
			 
			 images = new ArrayList<File>();
			 audios = new ArrayList<File>();
			 
			 // Initialize the first elements in images and audios and the step count to 1
			 images.add(0, null);
			 audios.add(0, null);
			 totalStep = 1;
			 
//			 //Create a new meta.txt file for the task
//			 metaInfo = new File(taskFolder.getAbsolutePath()+"/"+metaInfoFilename);
//			 try {
//				metaInfo.createNewFile();
//				
//				// Set the totalstep count in the file as 1
//				FileWriter fw = new FileWriter(metaInfo.getAbsoluteFile());
//				BufferedWriter bw = new BufferedWriter(fw);
//				bw.write(totalStep + "\n");
//				bw.close();
//			 } catch (IOException e) {
//				Log.d(TAG, "Meta file not created!");
//				e.printStackTrace();
//			 }
		 }
	}	
	
	public int getTotalSteps(){
		return totalStep;
	}
	
	public File getTaskFolder(){
		return taskFolder;
	}

	/**
	 * Read in all meta information, as well as image and audio files. 
	 */
	private void init() {		
		File[] files = taskFolder.listFiles();
		int maxStep = 0;
		for(int i = 0; i < files.length; i++){
			File file = files[i];
			String filename = file.getName();
			Log.d("Read In Task","filename:"+filename);
			String indexString = "";
			if(filename.startsWith("Image")){
				indexString = filename.substring(5,filename.length()-4);
				
			} else if(filename.startsWith("Recording")){
				indexString = filename.substring(9,filename.length()-4);
				
			}
			if(indexString!=""){
				maxStep = Math.max(Integer.parseInt(indexString),maxStep);
			} else {
				maxStep = 0;
			}
			Log.d("Read In Task","maxStep:"+maxStep);
		}
		
		images = new ArrayList<File>();
		audios = new ArrayList<File>();
		for(int i = 0; i <= maxStep; i ++){
			images.add(i, null);
			audios.add(i, null);
		}
		
		
		Log.d("Read In Task","Capacity:"+images.size());
		for(int i = 0; i < files.length; i++){
			File file = files[i];
			String filename = file.getName();
			String indexString = "";
			Log.d("Read In Task","filename:"+filename);
			
			if(filename.startsWith("Image")){
				indexString = filename.substring(5,filename.length()-4);
				int index = Integer.parseInt(indexString); 
				Log.d("Read In Task","Index:"+index);
				images.set(index, file);
			} else if(filename.startsWith("Recording")){
				indexString = filename.substring(9,filename.length()-4);
				int index = Integer.parseInt(indexString);
				Log.d("Read In Task","Index:"+index);
				audios.set(index, file);
			}			
		}
		totalStep = maxStep+1;
	}

	/**
	 * 
	 * @param index
	 * @return the Image file by the given index
	 */
	public File getImageFileByStep(int index){
		return images.get(index);
	}
	
	/**
	 * 
	 * @param index
	 * @return the Audio file by the given index
	 */
	File getAudioFileByStep(int index){
		return audios.get(index);
	}
	
	
	/**
	 * Generate meta file.
	 */
	public void saveTaskByMeta() {
		metaInfo = new File(taskFolder.getAbsolutePath()+"/"+metaInfoFilename);
		
		try {
			if(!metaInfo.exists()){
				if(!metaInfo.createNewFile()){

					Log.d("MetaInfo", "Create Meta Failed");
				};
			}
			FileWriter fw = new FileWriter(metaInfo,false);
			BufferedWriter bw = new BufferedWriter(fw);
			
			bw.append("TotalStep:"+totalStep).append('\n');
			Log.d("MetaInfo", "Append: "+ "TotalStep:"+totalStep);
			bw.append("Author:"+author).append('\n');
			Log.d("MetaInfo", "Append: "+ "Author:"+author);
			for(int i = 0; i < totalStep; i++){
				File imageFile = images.get(i);
				if(imageFile!=null){
					
					bw.append("Image:"+i+":"+imageFile.getName()).append('\n');
					Log.d("MetaInfo", "Append: "+ "Image:"+i+":"+images.get(i).getName());
				}
				File audioFile = audios.get(i);
				if(audioFile!=null){
					bw.append("Audio:"+i+":"+audioFile.getName()).append('\n');
					Log.d("MetaInfo", "Append: "+ "Audio:"+i+":"+audios.get(i).getName());
				}
			}
			bw.flush();
			bw.close();
		} catch (IOException e) {			
			e.printStackTrace();
		} 
	}
	
	/**
	 * Save this task into external storage.
	 */
	public void readTaskByMeta() {
		
		metaInfo = new File(taskFolder.getAbsolutePath()+"/"+metaInfoFilename);
		if(metaInfo.exists()){
			
			FileReader fr;
			images = new ArrayList<File>();
			audios = new ArrayList<File>();
			try {
				fr = new FileReader(metaInfo);
				BufferedReader br = new BufferedReader(fr);
				String line = br.readLine();
				while(line!=null){
					
					
					
					Log.d("MetaInfo", "Read: "+line);
					String[] args = line.split(":");
					for(int i = 0; i < args.length; i++){
						Log.d("MetaInfo", "Args["+i+"]="+args[i]);
					}
					if(args[0].equals("TotalStep")){
						
						totalStep = Integer.parseInt(args[1]);
						Log.d("MetaInfo", "Set Total step:"+totalStep);
						for(int i = 0; i < totalStep; i++){
							images.add(i,null);
							audios.add(i,null);
						}
					} else if (args[0].equals("Author")){
						
						author = args[1];
						Log.d("MetaInfo", "Set Author:"+author);
					} else if (args[0].equals("Image")){
						String imageFilePath = args[2];
						int imageStep = Integer.parseInt(args[1]);
						File imageFile = new File(taskFolder.getAbsolutePath()+"/"+imageFilePath);
						images.set(imageStep, imageFile);
						Log.d("MetaInfo", "Set Image at step "+imageStep+" : "+imageFile.getName());
					} else if (args[0].equals("Audio")){
						String audioFilePath = args[2];
						int audioStep = Integer.parseInt(args[1]);
						File audioFile = new File(taskFolder.getAbsolutePath()+"/"+audioFilePath);
						audios.set(audioStep, audioFile);
						Log.d("MetaInfo", "Set Audio at step "+audioStep+" : "+audioFile.getName());
					}
					line = br.readLine();
				}
				if(author==null||author.isEmpty()||author.equals("null")){
					author = GCMHelper.getRegId();
					Log.d("MetaInfo", "No author is found, put my name on it:"+author);
				} else {
					Log.d("MetaInfo", "Found author:"+author);
				}
				br.close();
				fr.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}		
	}
	
	
	public File renameAudioByStep(int currentStep, File audio){
		File newFile = new File(taskFolder.getAbsolutePath()+"/"+"Recording"+currentStep+".mp4");
		audio.renameTo(newFile);
		Log.d("MetaInfo", "Audio Renamed to :"+newFile.getName());
		return newFile;
		
	}
	
	public File renameImageByStep(int currentStep, File image){
		File newFile = new File(taskFolder.getAbsolutePath()+"/"+"Image"+currentStep+".jpg");
		image.renameTo(newFile);
		Log.d("MetaInfo", "Audio Renamed to :"+newFile.getName());
		return newFile;
	}
	
	

	/**
	 * Add the image file to the given step.
	 * @param currentStep
	 * @param image
	 */
	public void setImageByStep(int currentStep, File image) {
		Log.d("MetaInfo", "Set image to step "+currentStep+" :"+image.getName());
		images.set(currentStep, image);
	}
	


	/**
	 * Add the image file to the given step.
	 * @param currentStep
	 * @param audio
	 */
	public void setAudioByStep(int currentStep, File audio) {
		Log.d("MetaInfo", "Audios.set() :"+audio.getName());
		audios.set(currentStep, audio);		
		
	}

	public boolean addStepAfterCurrent(int currentStep, int mode) {
		Log.d("Next Step Exception","Current Step:"+currentStep+"  totalStep:"+totalStep);
		if((currentStep + 1 < totalStep) || (mode == TaskManager.VIEW_MODE)){
			// Just browsing through steps, don't increment totalStep 
			// or add null elements to images and audios
			return false;
		}
		else{
			// At the end of the tutorial. Add a new step.
			totalStep++;
			images.add(currentStep + 1, null);
			audios.add(currentStep + 1, null);
			return true;
		}		
	}

	public void renameTask(String name) {
		taskFolder.renameTo(new File(name));
	}

	public void deleteTask() {
		
		DeleteRecursive(taskFolder);
	}
	
	void DeleteRecursive(File fileOrDirectory) {
	    if (fileOrDirectory.isDirectory())
	        for (File child : fileOrDirectory.listFiles())
	            DeleteRecursive(child);

	    fileOrDirectory.delete();
	}

	public void receiveFile(DataInputStream dis) {
		try{
			String fileName = dis.readUTF();
			long fileLength = dis.readLong();
			Log.d("Send Tutorial", "Receive file:"+fileName+" "+fileLength);
			FileOutputStream fos = new FileOutputStream(taskFolder.getAbsolutePath()+"/"+fileName);
			byte[] bytes = new byte[(int)fileLength];
			dis.readFully(bytes);
			fos.write(bytes);
			fos.close();
			
		} catch (IOException e){
			
		}
		
	}

	public void sendTutorial() {
		Log.d("Send Tutorial", "Send tutorial");
		writeTutorialName();
		File[] files = taskFolder.listFiles();
		for(int i = 0; i < files.length; i++){
			writeFile(files[i]);
		}
		writeCompleted();

	}
	
	public void writeTutorialName(){
		Log.d("Send Tutorial", "Write tutorial name:"+taskName
				);
		BluetoothService mBluetoothService = BluetoothService.getInstance();
		mBluetoothService.writeTutorialName(taskName);
	}
	
	public void writeFile(File file){
		Log.d("Send Tutorial", "Write File:"+file.getName());
		BluetoothService mBluetoothService = BluetoothService.getInstance();
		mBluetoothService.writeFile(file);
	}
	
	public void writeCompleted(){
		Log.d("Send Tutorial", "Write Complete");
		BluetoothService mBluetoothService = BluetoothService.getInstance();
		mBluetoothService.writeCompleted();
	}

	public String getAuthorId() {
		Log.d("GCM Test", "Author id is "+author);
		
		return author;
	}
}
