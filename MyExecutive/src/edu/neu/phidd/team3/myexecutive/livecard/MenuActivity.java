package edu.neu.phidd.team3.myexecutive.livecard;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import edu.neu.phidd.team3.myexecutive.R;
import edu.neu.phidd.team3.myexecutive.gcm.GCMHelper;
import edu.neu.phidd.team3.myexecutive.videocall.AudioCall;
import edu.neu.phidd.team3.myexecutive.videocall.AudioCallUtil;

/**
 * The activity that opens a menu for Livecard.
 * @author yihuaqi
 *
 */
public class MenuActivity extends Activity{
	
	private static final String TAG = MenuActivity.class.getSimpleName();
	
	String userDeviceID;
	String chatSessionID;
	String learnerRegId;
	String msg;
	//AudioCallUtil mAudioCallUtil;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Get the user device id from the intent
		userDeviceID = getIntent().getStringExtra(GCMHelper.USER_DEVICE_ID);
		learnerRegId = getIntent().getStringExtra(GCMHelper.EXTRA_LEARNER_REGID);
		Log.d("GCM Test", "MenuActivity Learner regid is :"+learnerRegId);
		msg = getIntent().getStringExtra(GCMHelper.EXTRA_MESSAGE);
		
		//mAudioCallUtil = AudioCallUtil.getInstance();
		
	}

	@Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        openOptionsMenu();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        // Show a different menu if seeking permission from the helper
        if(msg.equals(GCMHelper.TUTORIAL_STARTED)){
        	inflater.inflate(R.menu.permission, menu);
        }
        else{
        	inflater.inflate(R.menu.livecard, menu);
        }
        
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection.
        switch (item.getItemId()) {
            case R.id.action_livecard_stop:
            	if(msg.equals(GCMHelper.QUICK_HELP)){
            		// Author has denied help. Send a message to the user
                	// to set the help flag at the user end to false
                	String target2 = learnerRegId;
    		    	GCMHelper.sendMessage(GCMHelper.NOT_NOW, target2, null, null, null);
            	}
            	stopService(new Intent(this, LiveCardService.class));
                return true;
                
            case R.id.action_livecard_help:
            	// Start the live chat session
            	Log.d(TAG, "Start live chat");


            	//startService(new Intent(this,LiveCardService.class).putExtras(getIntent().getExtras()).putExtra(GCMHelper.EXTRA_MESSAGE, "Helping learner..."));
            	
            	// Find ID of this device
            	final String deviceID = BluetoothAdapter.getDefaultAdapter().getAddress();
            	
            	// Generate chat session ID
            	chatSessionID = deviceID + userDeviceID;
            	
            	// Send a message to the user with the session ID to start the audio chat at the user end
    	    	String target = learnerRegId;
    	    	Log.d("GCM Test", "Learner regid is :"+learnerRegId);
    	    	GCMHelper.sendMessage(GCMHelper.VIDEO_CHAT, target, GCMHelper.VIDEO_CHAT_SESSION_ID, chatSessionID, null);
            	
            	Intent intent = new Intent(this, AudioCall.class);
            	Log.d(TAG, "Session ID at the helper side: " + chatSessionID);
            	intent.putExtra(GCMHelper.VIDEO_CHAT_SESSION_ID, chatSessionID);
    	    	startActivity(intent);
    	    	
            	// Start the audio call at the helper end    	    	
    	    	//mAudioCallUtil.startAudioCall(this, chatSessionID);
    	    	
            	return true;
            	
            case R.id.action_livecard_yes:
            	// Author has agreed to help. Send a message to the user
            	// to set the help flag at the user end to true
            	String target1 = learnerRegId;
		    	GCMHelper.sendMessage(GCMHelper.PERMISSION_GRANTED, target1, null, null, null);
            	
            	// Dismiss the live card
            	stopService(new Intent(this, LiveCardService.class));
            	return true;
            	
            case R.id.action_livecard_no:
            	// Author has denied help. Send a message to the user
            	// to set the help flag at the user end to false
            	String target3 = learnerRegId;
		    	GCMHelper.sendMessage(GCMHelper.HELP_DENIED, target3, null, null, null);
		    	
            	// Dismiss the live card
            	stopService(new Intent(this, LiveCardService.class));
            	return true;
            	
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onOptionsMenuClosed(Menu menu) {
    	// End the audio call
    	//mAudioCallUtil.endAudioCall(this);
    	
        // Nothing else to do, closing the activity.
        finish();
    }
}
