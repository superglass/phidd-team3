package edu.neu.phidd.team3.myexecutive.livecard;

import com.google.android.glass.timeline.LiveCard;
import com.google.android.glass.timeline.LiveCard.PublishMode;
import edu.neu.phidd.team3.myexecutive.R;
import edu.neu.phidd.team3.myexecutive.gcm.GCMHelper;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;

/**
 * The service is for publishing and rendering the Livecard.
 * @author yihuaqi
 *
 */
public class LiveCardService extends Service{
    private static final String LIVE_CARD_TAG = "LiveCardDemo";

    private LiveCard mLiveCard;
    private RemoteViews mLiveCardView;
    int requestCode = 0;
    
    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("Livecard Test","Livecard service created");
        
    }
    
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
    	/**
    	 * This method will get the message from the intent and render it on Livecard.
    	 */
    	String msg = intent.getStringExtra(GCMHelper.EXTRA_MESSAGE);
    	String learnerRegId = intent.getStringExtra(GCMHelper.EXTRA_LEARNER_REGID);
    	Log.d("GCM Test", "LiveCardService Learner regid is :"+learnerRegId);
    	Log.d("Livecard Test","On Start Command");
    	
    	// Unpublish the existing live card if any
    	if (mLiveCard != null && mLiveCard.isPublished()) {
            //Stop the handler from queuing more Runnable jobs
            

            mLiveCard.unpublish();
            mLiveCard = null;
            Log.d("Livecard Test","Unpublish Livecard");
        }
    	
        //if (mLiveCard == null) {
        Log.d("Livecard Test","Create LiveCard");
        // Get an instance of a live card
        mLiveCard = new LiveCard(this, LIVE_CARD_TAG);

        // Inflate a layout into a remote view
        mLiveCardView = new RemoteViews(getPackageName(), R.layout.livecard);
        
        // Set up the live card's action with a pending intent
        // to show a menu when tapped
        Intent menuIntent = new Intent(this, MenuActivity.class);
        menuIntent.putExtras(intent.getExtras());
        // Add the user device id to the menu intent
        menuIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        
        requestCode++;
        
        mLiveCard.setAction(PendingIntent.getActivity(this, 0, menuIntent, PendingIntent.FLAG_CANCEL_CURRENT));

        // To ask permission from the author to help
        if(msg.equals(GCMHelper.TUTORIAL_STARTED)){           
            // Set the message
            String tutorialName = intent.getStringExtra(GCMHelper.TUTORIAL_NAME);
            mLiveCardView.setTextViewText(R.id.livecard_message,"User started tutorial '" + tutorialName + "'. Do you want to help?");
        }
        else if(msg.equals(GCMHelper.TIME_UPDATE)){
        	String timeSpent = intent.getStringExtra(GCMHelper.TIME_SOFAR);
        	mLiveCardView.setTextViewText(R.id.livecard_message,"User spent " + timeSpent + " minutes in one step. Do you want to help?");
        }
        else if(msg.equals(GCMHelper.QUICK_HELP)){
        	// Set the message
        	String message = intent.getStringExtra(GCMHelper.USER_MESSAGE);
            mLiveCardView.setTextViewText(R.id.livecard_message,message);
        }

        // Publish the live card
        mLiveCard.publish(PublishMode.REVEAL);
        
        //Log.d("Livecard Test","Set text to:"+msg);
        //mLiveCardView.setTextViewText(R.id.livecard_message,msg);
        mLiveCard.setViews(mLiveCardView);

        return START_STICKY;       
    }

    @Override
    public void onDestroy() {
        if (mLiveCard != null && mLiveCard.isPublished()) {
            //Stop the handler from queuing more Runnable jobs
            

            mLiveCard.unpublish();
            mLiveCard = null;
            Log.d("Livecard Test","Unpublish Livecard");
        }
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
      /*
       * If you need to set up interprocess communication
       * (activity to a service, for instance), return a binder object
       * so that the client can receive and modify data in this service.
       *
       * A typical use is to give a menu activity access to a binder object
       * if it is trying to change a setting that is managed by the live card
       * service. The menu activity in this sample does not require any
       * of these capabilities, so this just returns null.
       */
       return null;
    }
}
