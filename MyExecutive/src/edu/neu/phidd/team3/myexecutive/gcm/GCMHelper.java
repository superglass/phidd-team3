package edu.neu.phidd.team3.myexecutive.gcm;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Sender;
import com.google.android.gms.gcm.GoogleCloudMessaging;

/**
 * This is the helper class for GCM.
 * One example of using this class is in {@link edu.neu.phidd.team3.myexecutive.demo.GCMDemo}
 * @author yihuaqi
 *
 */
public class GCMHelper {
	/**
	 * Project Number.
	 */
	final static String SENDER_ID = "134717269089";
	/**
	 * API Key
	 */
	final static String API_KEY = "AIzaSyCu8MPToRco0f9LG6kyD9U_uUYHvj3KtYI";
	static GoogleCloudMessaging gcm;
	
	/**
	 * Register Id
	 */
	static String regid;
	AtomicInteger msgId = new AtomicInteger();
    SharedPreferences prefs;
    
    /**
     * Key for putting and getting message from intent.
     */
    public static final String EXTRA_MESSAGE = "message";
    public static final String EXTRA_LEARNER_REGID = "learner regid";
    public static final String VIDEO_CHAT = "video_chat";
    public static final String USER_DEVICE_ID = "user_device_id";
    public static final String VIDEO_CHAT_SESSION_ID = "video_chat_session_id";
    public static final String PROPERTY_REG_ID = "registration_id";
    public static final String TUTORIAL_STARTED = "tutorial_started";
    public static final String TUTORIAL_NAME = "tutorial_name";
    public static final String PERMISSION_GRANTED = "permission_granted";
    public static final String TIME_UPDATE = "time_update";
    public static final String TIME_SOFAR = "time_sofar";
    public static final String HELP_DENIED = "help_denied";
    public static final String NOT_NOW = "not_now";
    public static final String USER_MESSAGE = "user_message";
    public static final String QUICK_HELP = "quick_help";
    
    static final String TAG = "GCM Test";
    static String HOWTO_PREFERENCE = "HOWTO";
    
    
	//API key for application from the API console
	
	private static int countCollapseKey=0;
	// This is a *cheat*  It is a hard-coded registration ID from an Android device
	// that registered itself with GCM using the same project id shown above.
	

	private static List<String> androidTargets = new ArrayList<String>();


    /**
     * This method must be called in onCreate() to register the device;
     * @param activity
     */
    public static void onCreate(Activity activity){
    	gcm = GoogleCloudMessaging.getInstance(activity);
		regid = getRegistrationId(activity);
		Log.d(TAG, "Server regid:"+regid);
		if(regid.isEmpty()){
			registerInBackground(activity);
		} 
		
		
    }
    
    public static String getRegId(){
    	Log.d("MetaInfor", "Get Server regid:"+regid);
    	return regid;
    }

	/**
	 * Registers the application with GCM servers asynchronously.
	 * <p>
	 * Stores the registration ID and app versionCode in the application's
	 * shared preferences.
	 */
	private static void registerInBackground(final Activity activity) {
	    new AsyncTask<Void,String,String>() {
	        @Override
	        protected String doInBackground(Void... params) {
	            String msg = "";
	            try {
	                if (gcm == null) {
	                    gcm = GoogleCloudMessaging.getInstance(activity);
	                }
	                regid = gcm.register(SENDER_ID);
	                msg = "Device registered, registration ID=" + regid;
	                Log.d(TAG, msg);

	                // For this demo: we don't need to send it because the device
	                // will send upstream messages to a server that echo back the
	                // message using the 'from' address in the message.

	                // Persist the regID - no need to register again.
	                storeRegistrationId(activity, regid);
	            } catch (IOException ex) {
	                msg = "Error :" + ex.getMessage();
	                Log.d(TAG, msg);
	                // If there is an error, don't just keep trying to register.
	                // Require the user to click a button again, or perform
	                // exponential back-off.
	            }
	            return msg;
	        }

	        /**
	         * Stores the registration ID and app versionCode in the application's
	         * {@code SharedPreferences}.
	         *
	         * @param context application's context.
	         * @param regId registration ID
	         */
	        private void storeRegistrationId(Activity activity, String regId) {
	            final SharedPreferences prefs = getGCMPreferences(activity);
	            
	            
	            SharedPreferences.Editor editor = prefs.edit();
	            editor.putString(PROPERTY_REG_ID, regId);
	            
	            editor.commit();
	        }

			@Override
	        protected void onPostExecute(String msg) {
	            
	        }


	    }.execute(null, null, null);
	    
	    
	}
    

	
	/**
	 * Gets the current registration ID for application on GCM service.
	 * <p>
	 * If result is empty, the app needs to register.
	 *
	 * @return registration ID, or empty string if there is no existing
	 *         registration ID.
	 */
	public static String getRegistrationId(Activity activity) {
	    final SharedPreferences prefs = getGCMPreferences(activity);
	    String registrationId = prefs.getString(PROPERTY_REG_ID, "");
	    if (registrationId.isEmpty()) {
	        Log.i(TAG, "Registration not found.");
	        return "";
	    }
	    // Check if app was updated; if so, it must clear the registration ID
	    // since the existing regID is not guaranteed to work with the new
	    // app version.
	    
	    return registrationId;
	}
	
	/**
	 * @return Application's {@code SharedPreferences}.
	 */
	private static SharedPreferences getGCMPreferences(Activity activity) {
	    // This sample app persists the registration ID in shared preferences, but
	    // how you store the regID in your app is up to you.
	    return activity.getSharedPreferences(HOWTO_PREFERENCE,
	            Context.MODE_PRIVATE);
	}
	
	/**
	 * Send the message to given target
	 * @param message A brief message
	 * @param target regId of the target device
	 */
	public static void sendMessage(String message, String target, String key, String value, String userMessage){
		new SendMessageTask(message,target, key, value, userMessage).execute();
	}
	
	public static class SendMessageTask extends AsyncTask<Void, Void, Void>{
		String msg;
		String ANDROID_DEVICE;
		String key;
		String value;
		String userMsg;
		public SendMessageTask(String message, String target, String key, String value, String userMsg) {
			this.msg = message;
			this.ANDROID_DEVICE = target;
			this.key = key;
			this.value = value;
			this.userMsg = userMsg;
		}
		@Override
		protected Void doInBackground(Void... params) {
        	String collapseKey = "";
        	String userMessage = "";
        	Log.d(TAG,"Prepare to send message");
        	try {
        		userMessage = "Message";
        		collapseKey = "CollapseKey"+countCollapseKey;
        	} catch (Exception e) {
        		e.printStackTrace();
        		//return null;
        	}
        	androidTargets.clear();
        	androidTargets.add(ANDROID_DEVICE);
			Sender sender = new Sender(API_KEY);
			Message message = new Message.Builder()
			.collapseKey(collapseKey)
			.timeToLive(30)
			.delayWhileIdle(false)
			.addData(EXTRA_MESSAGE, msg)
			.addData(EXTRA_LEARNER_REGID, regid)
			.addData(key, value)
			.addData(USER_MESSAGE, userMsg)
			.build();
			Log.d("GCM Test", "GCMHelper Learner regid is :"+regid);
			try{
				MulticastResult result = sender.send(message, androidTargets, 10);
				if(result.getResults() !=null){
					int canonicalRegId = result.getCanonicalIds();
					if(canonicalRegId !=0){
						Log.d(TAG,"CanonicalRegId:"+canonicalRegId);			
					} else {
						int error = result.getFailure();
						Log.i(TAG,"Broadcast failure "+error);
					}
					countCollapseKey++;
				}
			} catch (Exception exception ){
				Log.d(TAG,"Send exception:"+exception.getMessage());
			}
			return null;
		}
		
		/**
		@Override
		protected Void doInBackground(Void... params) {
			try {
				Bundle data = new Bundle();
				data.putString("my_message", message);
				String id = Integer.toString(msgId.incrementAndGet());
				gcm.send(SENDER_ID+"@gcm.googleapis.com", id, data);
				Log.d(TAG, "Sent message");
			} catch (IOException e){
				Log.d(TAG, "Error:"+e.getMessage());
			}
			
			return null;
		}
		*/
		
	}


    
    
}
