package edu.neu.phidd.team3.myexecutive.bluetooth;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import com.google.android.glass.view.WindowUtils;
import edu.neu.phidd.team3.myexecutive.R;
import edu.neu.phidd.team3.myexecutive.task.TaskManager;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.TextView;

public class SendTutorialActivity extends Activity{
	TaskManager mTaskManager;
	TextView mInstructionText;
	TextView mStatusText;
	BluetoothAdapter mBluetoothAdapter;
	BluetoothService mBluetoothService;
	List<BluetoothDevice> discorveredDevices;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		getWindow().requestFeature(WindowUtils.FEATURE_VOICE_COMMANDS);
		mTaskManager = TaskManager.getInstance();
		super.onCreate(savedInstanceState);
		discorveredDevices = new ArrayList<BluetoothDevice>();
		setContentView(R.layout.activity_sendtutorial);
		mInstructionText = (TextView) findViewById(R.id.sendTutorial_Instruction_Text);
		mStatusText = (TextView) findViewById(R.id.sendTutorial_Status_Text);
		if(mTaskManager.getMode() == TaskManager.SEND_MODE){
			mInstructionText.setText("Tap to select devices");
		} else if (mTaskManager.getMode() == TaskManager.RECEIVE_MODE){
			mInstructionText.setText("Waiting for Helper");
		}
		
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if(!mBluetoothAdapter.isEnabled()){
			mInstructionText.setText("Bluetooth is not enabled. Please pair the devices first");
		} else {
			if(mBluetoothService == null){
				mBluetoothService = BluetoothService.getInstance();
				
				
			}
		}
		mReceiver = new BTBroadcastReceiver();
		IntentFilter iFilter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
		this.registerReceiver(mReceiver, iFilter);
	}

	BTBroadcastReceiver mReceiver;
	
	@Override
	protected void onResume() {
		
		super.onResume();
		
		
		mBluetoothService.setHandler(mHandler);
		mBluetoothService.start();
	}



	@Override
	protected void onPause() {

		super.onPause();
		
	}
	
	@Override
	protected void onStop(){
		super.onStop();
		this.unregisterReceiver(mReceiver);

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_DPAD_CENTER){
			openOptionsMenu();
			return true;
		}
		if(keyCode == KeyEvent.KEYCODE_BACK){
			
			finish();
		}
		return false;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		if(item.getItemId() == DISCOVER_DEVICES_ID){
			discorveredDevices.clear();
			if(mBluetoothAdapter.isDiscovering()){
				mBluetoothAdapter.cancelDiscovery();
			}
			mBluetoothAdapter.startDiscovery();
			mStatusText.setText("Discorvering Devices...");
		} else {
			String itemName = (String) item.getTitle();
			BluetoothDevice selectedDevice = findDeviceByName(itemName);
			mBluetoothService.connect(selectedDevice, true);
		}
		return super.onMenuItemSelected(featureId, item);
	}

	private BluetoothDevice findDeviceByName(String itemName) {
		Set<BluetoothDevice> devices = mBluetoothAdapter.getBondedDevices();
		Iterator<BluetoothDevice> it = devices.iterator();
		while(it.hasNext()){
			BluetoothDevice device = it.next();
			if(device.getName().equals(itemName)){
				Log.d("Send Tutorial", "Selected Device:"+itemName);
				return device; 
			}
		}
		it = discorveredDevices.iterator();
		while(it.hasNext()){
			BluetoothDevice device = it.next();
			if(device.getName().equals(itemName)){
				Log.d("Send Tutorial", "Selected Device:"+itemName);
				return device; 
			}
		}
		return null;
		
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		
		populateMenu(menu);
		return true;
	}
	
	private static final int DISCOVER_DEVICES_ID = -1; 
	private void populateMenu(Menu menu) {
		menu.clear();
		menu.add(Menu.NONE, DISCOVER_DEVICES_ID, Menu.NONE, "Discover New Devices");
		Set<BluetoothDevice> devices = mBluetoothAdapter.getBondedDevices();
		
		Iterator<BluetoothDevice> it = devices.iterator();
		addAllDevicesToMenu(it,menu);
		it = discorveredDevices.iterator();
		addAllDevicesToMenu(it,menu);

	}

	private void addAllDevicesToMenu(Iterator<BluetoothDevice> it, Menu menu) {
		while(it.hasNext()){
			BluetoothDevice device = it.next();
			String deviceName = device.getName();
			Log.d("Send Tutorial", "Add to menu:"+deviceName);
			
			
			
			menu.add(deviceName);
		}
		
	}

	private Handler mHandler = new Handler(){

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what){
			case BluetoothService.MSG_ACCEPTED:
				String deviceName = msg.getData().getString(BluetoothService.MSGKEY_DEVICE_NAME);
				if(mTaskManager.getMode() == TaskManager.RECEIVE_MODE){
					mStatusText.setText("Accepted connection from "+deviceName);
				}
				break;
			case BluetoothService.MSG_ACCEPTING:
				if(mTaskManager.getMode() == TaskManager.RECEIVE_MODE){
					mStatusText.setText("Accepting connectiong...");
				}
				break;
			case BluetoothService.MSG_CONNECTED:
				deviceName = msg.getData().getString(BluetoothService.MSGKEY_DEVICE_NAME);
				if(mTaskManager.getMode() == TaskManager.SEND_MODE){
					mStatusText.setText("Connected with "+deviceName);
				}
				if(mTaskManager.getMode() == TaskManager.SEND_MODE){
					mTaskManager.sendTutorial();
				}
				break;
			case BluetoothService.MSG_CONNECTING:
				deviceName = msg.getData().getString(BluetoothService.MSGKEY_DEVICE_NAME);
				if(mTaskManager.getMode() == TaskManager.SEND_MODE){
					mStatusText.setText("Try to connect with "+deviceName);
				}
				break;
			case BluetoothService.MSG_SENDING_FILE:
				String fileName = msg.getData().getString(BluetoothService.MSGKEY_FILE_NAME);
				mStatusText.setText("Sending file "+fileName);
				break;
			case BluetoothService.MSG_SENDING_TUTORIAL:
				String tutorialName = msg.getData().getString(BluetoothService.MSGKEY_TUTORIAL_NAME);
				mStatusText.setText("Seding tutorial "+tutorialName);
				break;
			case BluetoothService.MSG_SENT_FILE:
				fileName = msg.getData().getString(BluetoothService.MSGKEY_FILE_NAME);
				mStatusText.setText("Finish sending file "+fileName);
				break;
			case BluetoothService.MSG_SENT_TUTORIAL:
				
				mStatusText.setText("Finish sending tutorial ");
				break;
			}
			super.handleMessage(msg);
		}
		
	};
	
	private class BTBroadcastReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if(action.equals(BluetoothDevice.ACTION_FOUND)){
				BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				discorveredDevices.add(device);
				mStatusText.setText("Found new devices");
				
			}
			
		}
		
	}
	
}
