package edu.neu.phidd.team3.myexecutive.bluetooth;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import edu.neu.phidd.team3.myexecutive.audio.InstructionManager;
import edu.neu.phidd.team3.myexecutive.task.TaskManager;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

/**
 * This class handles bluetooth communication.
 * 
 * @author YiH
 * 
 */
public class BluetoothService {

	private final BluetoothAdapter mAdapter;
	private Handler mHandler;
	private AcceptThread mAcceptThread;

	private ConnectThread mConnectThread;
	private ConnectedThread mConnectedThread;

	private int mState;
	// Name for the SDP record when creating server socket
	private static final String NAME_SECURE = "BluetoothTestSecure";
	private static final String NAME_INSECURE = "BluetoothTestInsecure";

	// Unique UUID for this application
	private static final UUID MY_UUID_SECURE = UUID
			.fromString("fa87c0d0-ffff-11de-8a39-0800200c9a69");
	private static final UUID MY_UUID_INSECURE = UUID
			.fromString("8ce255c0-eeee-11e0-ac64-0800200c9a69");

	// Types of Connection state
	public static final int STATE_NONE = 0; // we're doing nothing
	public static final int STATE_LISTEN = 1; // now listening for incoming
												// connections
	public static final int STATE_CONNECTING = 2; // now initiating an outgoing
													// connection
	public static final int STATE_CONNECTED = 3; // now connected to a remote
													// device
	public static final String TAG = "BS tag";
	
	public static final int MSG_CONNECTING = 0;
	public static final int MSG_CONNECTED = 1;
	public static final int MSG_SENDING_TUTORIAL = 2;
	public static final int MSG_SENDING_FILE = 3;
	public static final int MSG_SENT_FILE = 4;
	public static final int MSG_ACCEPTING = 5;
	public static final int MSG_ACCEPTED = 6;
	public static final int MSG_SENT_TUTORIAL = 7;
	public static final int MSG_RECEIVED_TUTORIAL = 8;
	
	
	public static final char ACTION_SEND_FILE = 'f';
	public static final char ACTION_COMPLETE = 'c';
	public static final char ACTION_TUTORIAL_NAME = 't';
	public static final String MSGKEY_DEVICE_NAME = "device name";
	public static final String MSGKEY_FILE_NAME = "file name";
	public static final String MSGKEY_TUTORIAL_NAME = "tutorial name";

	// Singleton of this class.
	private static BluetoothService instance = null;

	/**
	 * Initialize the BluetoothService in a singleton way.
	 * 
	 * @param handler
	 */
	private BluetoothService() {
		mAdapter = BluetoothAdapter.getDefaultAdapter();

	}

	/**
	 * Initialize the BluetoothService with given handler.
	 * 
	 * @param handler
	 */
	public static synchronized void syncInit(Handler handler) {
		if (instance == null) {
			instance = new BluetoothService();
			instance.setHandler(handler);
		}
	}

	public void setHandler(Handler handler) {
		mHandler = handler;
	}

	/**
	 * Return a BluetoothService singleton. If syncInit is not called before.
	 * Then a BluetoothService singleton without handler will be created.
	 * 
	 * @return
	 */
	public static BluetoothService getInstance() {
		if (instance == null) {
			syncInit(null);
		}
		return instance;
	}

	/**
	 * Set the current state of the bluetooth connection
	 * 
	 * @param state
	 *            An integer defining the current connection state.
	 */
	private void setState(int state) {
		Log.d(TAG, "setState()" + mState + "->" + state);
		mState = state;
	}

	/**
	 * Start the bluetooth connection. Specifically start AcceptThread to begin
	 * a session in listening (server) mode. Called by
	 */
	public void start() {
		if (mConnectThread != null) {
			mConnectThread.cancle();
			mConnectThread = null;
		}

		if (mConnectedThread != null) {
			mConnectedThread.cancle();
			mConnectedThread = null;
		}

		setState(STATE_LISTEN);

		if (mAcceptThread == null) {
			mAcceptThread = new AcceptThread(true);
			mAcceptThread.start();
		}

	}

	/**
	 * Start the ConnectThread to initiate a connection to a remote device.
	 * 
	 * @param device
	 *            The BluetoothDevice to connect
	 * @param secure
	 *            Socket Security type - Secure (true), Insecure (false)
	 */
	public void connect(BluetoothDevice device, boolean secure) {

		// Cancel any thread attempting to make a connection
		if (mState == STATE_CONNECTING) {
			if (mConnectedThread != null) {
				mConnectThread.cancle();
				mConnectThread = null;
			}
		}

		// Cancel any thread currently running a connection
		if (mConnectedThread != null) {
			mConnectedThread.cancle();
			mConnectedThread = null;
		}

		// Start the thread to connect with the given device
		mConnectThread = new ConnectThread(device, secure);
		mConnectThread.start();

		setState(STATE_CONNECTING);

	}

	/**
	 * Start the ConnectedThread to begin managing a Bluetooth connection
	 * 
	 * @param socket
	 *            The BluetoothSocket on which the connection was made
	 * @param device
	 *            The bluetoothDevice that has been connected
	 * @param socketType
	 *            The socket type.
	 */
	public void connected(BluetoothSocket socket, BluetoothDevice device,
			final String socketType) {

		// Cancel the thread that completed the connection
		if (mConnectThread != null) {
			mConnectThread.cancle();
			mConnectThread = null;
		}

		// Cancel any thread currently running a connection
		if (mConnectedThread != null) {
			mConnectedThread.cancle();
			mConnectedThread = null;
		}

		// Cancel the accept thread because we only want to connect to one
		// device
		if (mAcceptThread != null) {
			mAcceptThread.cancle();
			mAcceptThread = null;
		}

		// Start the thread to manage the connection and perform trasmission
		mConnectedThread = new ConnectedThread(socket, socketType);
		mConnectedThread.start();

		// Send the name of the connected device back to the UI activity
		if (mHandler != null) {

			Message msg = mHandler.obtainMessage(MSG_CONNECTED);
			Bundle bundle = new Bundle();
			bundle.putString(MSGKEY_DEVICE_NAME, device.getName());
			msg.setData(bundle);
			mHandler.sendMessage(msg);
		}
		setState(STATE_CONNECTED);
	}

	/**
	 * Stop all threads
	 */
	public void stop() {
		Log.d("BS tag", "Something stopped");
		if (mConnectThread != null) {
			mConnectThread.cancle();
			mConnectThread = null;
		}

		if (mConnectedThread != null) {
			mConnectedThread.cancle();
			mConnectedThread = null;
		}

		if (mAcceptThread != null) {
			mAcceptThread.cancle();
			mAcceptThread = null;
		}
		setState(STATE_NONE);
	}

	/**
	 * Indicate that the connection attempt failed and notify the UI Activity
	 */
	private void connectionFailed() {
		Log.d(TAG, "Connection Failed");

		// Start the service over to restart listening mode
		BluetoothService.this.start();
	}

	private void connectionLost() {
		Log.d(TAG, "Connection Lost");

		// Start the service over to restart listening mode
		BluetoothService.this.start();
	}

	/**
	 * This thread runs while listening for incoming connnections. It behave
	 * like a server-side client. It runs until a connection is accepted (or
	 * until cancelled).
	 * 
	 * @author YiH
	 * 
	 */
	class AcceptThread extends Thread {

		// The local server socket
		final BluetoothServerSocket mmServerSocket;
		String mSocketType;

		public AcceptThread(boolean secure) {
			BluetoothServerSocket tmp = null;
			mSocketType = secure ? "Secure" : "Insecure";

			// Create a new listening server socket
			try {
				if (secure) {
					tmp = mAdapter.listenUsingRfcommWithServiceRecord(
							NAME_SECURE, MY_UUID_SECURE);
				} else {
					tmp = mAdapter.listenUsingInsecureRfcommWithServiceRecord(
							NAME_INSECURE, MY_UUID_INSECURE);
				}

			} catch (IOException e) {
				Log.e(TAG, "Socket Type: " + mSocketType + "listen() failed", e);
			}
			mmServerSocket = tmp;
		}

		public void run() {
			setName("AcceptThread" + mSocketType);
			BluetoothSocket socket = null;
			if (mHandler != null) {

				Message msg = mHandler.obtainMessage(MSG_ACCEPTING);
				mHandler.sendMessage(msg);
			}
			// Listen to the server socket if we're not connected
			while (mState != STATE_CONNECTED) {
				try {
					/*
					 * This is a blocking call and will only return on a
					 * successfull connection or an exception
					 */
					socket = mmServerSocket.accept();
				} catch (IOException e) {
					Log.e(TAG, "Socket Type: " + mSocketType
							+ "accept() failed", e);
					break;
				}

				// If a connection was accepted
				if (socket != null) {
					synchronized (BluetoothService.this) {
						switch (mState) {
						case STATE_LISTEN:
						case STATE_CONNECTING:
							// Situation normal. Start the connected thread.
							connected(socket, socket.getRemoteDevice(),
									mSocketType);
							if (mHandler != null) {

								Message msg = mHandler.obtainMessage(MSG_ACCEPTED);
								Bundle bundle = new Bundle();
								bundle.putString(MSGKEY_DEVICE_NAME, socket.getRemoteDevice().getName());
								msg.setData(bundle);
								mHandler.sendMessage(msg);
							}
							break;
						case STATE_NONE:
						case STATE_CONNECTED:
							// Either not ready or already connected. Terminate
							// new socket.
							try {
								socket.close();
							} catch (IOException e) {
								Log.e(TAG, "Could not close unwanted socket", e);
							}
							break;
						}
					}
				}
			}
		}

		public void cancle() {
			try {
				mmServerSocket.close();
			} catch (IOException e) {
				Log.e(TAG, "Socket Type" + mSocketType
						+ "close() of server failed", e);
			}
		}
	}

	/**
	 * This thread runs while attempting to make an outgoing connection with a
	 * device. It runs straight through; the connection either succeeds or fails
	 * 
	 * @author YiH
	 * 
	 */
	class ConnectThread extends Thread {
		private final BluetoothSocket mmSocket;
		private final BluetoothDevice mmDevice;
		private String mSocketType;

		public ConnectThread(BluetoothDevice device, boolean secure) {
			mmDevice = device;
			BluetoothSocket tmp = null;
			mSocketType = secure ? "Secure" : "Insecure";

			// Get a BluetoothSocket for a connection with the given
			// BluetoothDevice

			try {
				if (secure) {
					tmp = device
							.createRfcommSocketToServiceRecord(MY_UUID_SECURE);
				} else {
					tmp = device
							.createInsecureRfcommSocketToServiceRecord(MY_UUID_INSECURE);
				}
			} catch (IOException e) {
				Log.e(TAG, "Socket Type: " + mSocketType + "create() failed", e);
			}
			mmSocket = tmp;
		}

		public void run() {
			setName("ConnectThread" + mSocketType);

			// Always cancel discovery because it will slow down a connection
			mAdapter.cancelDiscovery();

			// Make a connection to the BluetoothSocket
			try {
				// This is a blocking call and will only return on a
				// successful connection or an exception
				if (mHandler != null) {

					Message msg = mHandler.obtainMessage(MSG_CONNECTING);
					Bundle bundle = new Bundle();
					bundle.putString(MSGKEY_DEVICE_NAME, mmDevice.getName());
					msg.setData(bundle);
					mHandler.sendMessage(msg);
				}
				
				mmSocket.connect();

			} catch (IOException e) {
				// Close the socket
				try {
					mmSocket.close();
				} catch (IOException e2) {
					Log.e(TAG, "unable to close() " + mSocketType
							+ " socket during connection failure", e2);
				}
				connectionFailed();
				return;
			}

			// Reset the ConnectThread because we're done
			synchronized (BluetoothService.this) {
				mConnectThread = null;

			}

			// Start the connected thread
			connected(mmSocket, mmDevice, mSocketType);
		}

		public void cancle() {
			try {
				mmSocket.close();
			} catch (IOException e) {
				Log.e(TAG, "close() of connect " + mSocketType
						+ " socket failed", e);
			}
		}

	}

	/**
	 * This thread runs during a connection with a remote device It handles all
	 * incoming and outgoing transmissions.
	 * 
	 * @author YiH
	 * 
	 */
	class ConnectedThread extends Thread {
		private final BluetoothSocket mmSocket;
		private final InputStream mmInStream;
		private final OutputStream mmOutStream;
		private final DataOutputStream dos;
		private final DataInputStream dis;

		// Types of status.
		private final int WAITING = 0;
		private final int RECEIVING_TUTORIAL = 1;
		private final int RECEIVING_FILE = 2;
		
		int state;
		char stateChar;
		boolean transferringCompleted = false;
		
		TaskManager mTaskManager;

		public ConnectedThread(BluetoothSocket socket, String socketType) {
			Log.d(TAG, "create ConnectedThread: " + socketType);
			mmSocket = socket;
			InputStream tmpIn = null;
			OutputStream tmpOut = null;
			
			// Get the BluetoothSocket input and output streams
			try {
				tmpIn = socket.getInputStream();
				tmpOut = socket.getOutputStream();
			} catch (IOException e) {
				Log.e(TAG, "temp sockets not created", e);
			}

			mmInStream = tmpIn;
			mmOutStream = tmpOut;
			dos = new DataOutputStream(mmOutStream);
			dis = new DataInputStream(mmInStream);
			mTaskManager = TaskManager.getInstance();
		}

		public void run() {

			
			
			
			// Keep listening to the DataInputStream while connected
			while (!transferringCompleted) {
				try {
					// Read from the DataInputStream
					switch (state) {
					case WAITING:
						stateChar = dis.readChar();
						
						if (stateChar == ACTION_SEND_FILE) {
							Log.d("Send tutorial","Receive character:"+stateChar);

							state = RECEIVING_FILE;
						} else if (stateChar == ACTION_COMPLETE) {
							Log.d("Send tutorial","Receive character:"+stateChar);
							InstructionManager.getInstance().play("Tutorial received");
							if(mHandler!=null){
								Message msg = mHandler.obtainMessage(MSG_SENDING_TUTORIAL);
								
								mHandler.sendMessage(msg);
							}
							transferringCompleted = true;
						} else if (stateChar == ACTION_TUTORIAL_NAME){
							Log.d("Send tutorial","Receive character:"+stateChar);
							state = RECEIVING_TUTORIAL;
						}
						

						break;
					case RECEIVING_FILE:
						TaskManager.getInstance().receiveFile(dis);
						state = WAITING;
						break;
					case RECEIVING_TUTORIAL:
						String taskName = dis.readUTF();
						TaskManager.getInstance().createTaskInfo(taskName);
						state = WAITING;
						break;
					default:
						break;
					}

				} catch (IOException e) {
					Log.e(TAG, "disconnected", e);
					connectionLost();
					// Start the service over to restart listening mode
					BluetoothService.this.start();
					break;
				}

			}

		}


		public void cancle() {
			try {
				mmSocket.close();
			} catch (IOException e) {
				Log.e(TAG, "close() of connect socket failed", e);
			}

		}




		public void writeTutorialName(String tutorialName) {
			if(mHandler!=null){
				Message msg = mHandler.obtainMessage(MSG_SENDING_TUTORIAL);
				Bundle bundle = new Bundle();
				bundle.putString(MSGKEY_TUTORIAL_NAME, tutorialName);
				msg.setData(bundle);
				mHandler.sendMessage(msg);
			}
			
			
			try {
				dos.writeChar(ACTION_TUTORIAL_NAME);
				dos.writeUTF(tutorialName);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}

		public void writeFile(File file) {
			if(mHandler!=null){
				Message msg = mHandler.obtainMessage(MSG_SENDING_FILE);
				Bundle bundle = new Bundle();
				bundle.putString(MSGKEY_FILE_NAME, file.getName());
				msg.setData(bundle);
				mHandler.sendMessage(msg);
			}
			
			try{
				dos.writeChar(ACTION_SEND_FILE);
				dos.writeUTF(file.getName());
				dos.writeLong(file.length());
				
				Log.d("Send Tutorial", "Sending File:"+file.getName()+" "+file.length());
				FileInputStream fis = new FileInputStream(file);
				
				byte[] bytes = new byte[(int) file.length()];
				fis.read(bytes);
				dos.write(bytes);
				fis.close();
			} catch (IOException e){
				e.printStackTrace();
			}
			
			if(mHandler!=null){
				Message msg = mHandler.obtainMessage(MSG_SENT_FILE);
				Bundle bundle = new Bundle();
				bundle.putString(MSGKEY_DEVICE_NAME, file.getName());
				msg.setData(bundle);
				mHandler.sendMessage(msg);
			}
		}

		public void writeCompleted() {
			try {
				dos.writeChar(ACTION_COMPLETE);
				if(mHandler!=null){
					Message msg = mHandler.obtainMessage(MSG_SENT_TUTORIAL);
					mHandler.sendMessage(msg);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	

	}



	public void writeTutorialName(String tutorialName) {
		ConnectedThread r;
		synchronized (this) {
			if (mState != STATE_CONNECTED)
				return;
			r = mConnectedThread;

		}
		r.writeTutorialName(tutorialName);

	}
	
	public void writeFile(File file) {
		ConnectedThread r;
		synchronized (this) {
			if (mState != STATE_CONNECTED)
				return;
			r = mConnectedThread;

		}
		r.writeFile(file);		
	}
	
	public void writeCompleted() {
		ConnectedThread r;
		synchronized (this) {
			if (mState != STATE_CONNECTED)
				return;
			r = mConnectedThread;

		}
		r.writeCompleted();
	}

}
