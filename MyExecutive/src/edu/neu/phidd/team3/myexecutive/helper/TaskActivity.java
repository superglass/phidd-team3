package edu.neu.phidd.team3.myexecutive.helper;

import java.io.File;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.glass.content.Intents;
import com.google.android.glass.view.WindowUtils;
import com.google.android.glass.widget.CardScrollAdapter;
import com.google.android.glass.widget.CardScrollView;
import edu.neu.phidd.team3.myexecutive.AudioRecordActivity;
import edu.neu.phidd.team3.myexecutive.R;
import edu.neu.phidd.team3.myexecutive.audio.InstructionManager;
import edu.neu.phidd.team3.myexecutive.audio.PlayAudio;
import edu.neu.phidd.team3.myexecutive.gcm.GCMHelper;
import edu.neu.phidd.team3.myexecutive.speech.SpeechToText;
import edu.neu.phidd.team3.myexecutive.task.TaskManager;
import edu.neu.phidd.team3.myexecutive.videocall.AudioCallUtil;

public class TaskActivity extends Activity{
	
	private static final String TAG = TaskActivity.class.getSimpleName();
	private static final int REQUEST_IMAGE_CAPTURE = 1;
	private static final int REQUEST_RECORD_AUDIO = 2;
	//public static final String HUAQI_GLASS = "APA91bEK7qsi_L_YrYsPakAZvpeN2lU3oDWHQIl78Uh4NNAKKcz6hnp_-nVDYLdUupZSYpslzvBlRltI9uUYdW7vRgr8uSHtzjdHu2wmBYoKkzeSpG_Lq5t2_qWGcLXm-RhAeXP45C2tf02XueBcsyuWpG-Ne6sxYYOnp--41ivGWlDHc4Y84Vs";
	//public static final String ANJU_GLASS = "APA91bGV9wrJnK4rcSUA1lzkaFSFsKzdlFl6N4j-UKHqps5AdatClPmcWf90LK--7PvoCLCxMAggxm3hSvt3j4B-tLeHp56S8mSQqkqah1dAJpNWofwWx7BpCVWQe8Fsq9IEfiQeQu1_Z5LlNv6-JYW-tpF-xZskbLMsf3kxCQanxEvs2Vd_F_0";
	
	TaskManager mTaskManager;
	InstructionManager mInstructionManager;
	
	CardScrollView mCardScrollView;
	StepCardScrollAdapter mStepCardScrollAdapter;
	SpeechToText stt;
	View mCustomizedStepCardView = null;
	
	AudioChatReceiver mAudioChatReceiver;
	MessageReceiver mMessageReceiver;
	
	boolean audioWithPicture = true;
	boolean stopAudioFlag = false;
	
	public static boolean helpSetting;
	private int state = 0;
	private CountDownTimer cdTimer;
	long startTime = 60 * 1000; //10 * 1000;
	long interval = 1 * 1000;
	long timeSpentInStep = 0;
	Activity thisActivity;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		thisActivity = this;
		
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		getWindow().requestFeature(WindowUtils.FEATURE_VOICE_COMMANDS);
		stt = new SpeechToText(this);
		mTaskManager = TaskManager.getInstance();
		mInstructionManager = InstructionManager.getInstance();

		state = mTaskManager.getMode();
		
		mCardScrollView = new CardScrollView(this);
		mStepCardScrollAdapter = new StepCardScrollAdapter();
		mCardScrollView.setAdapter(mStepCardScrollAdapter);
		mCardScrollView.activate();
		CardItemSelectedListener listener = new CardItemSelectedListener();
		mCardScrollView.setOnItemSelectedListener(listener);
		
		setContentView(mCardScrollView);
		listener.onItemSelected(mCardScrollView, mCardScrollView.getSelectedView(), 0, 0);
		
		helpSetting = true;
		// If in VIEW mode, notify the author that the user has started using the tutorial
		if(mTaskManager.getMode() == TaskManager.VIEW_MODE){
			if(mTaskManager.getAuthorId().equals(GCMHelper.getRegId())){
				// The tutorial is created by the user itself
				// Don't do anything
			}
			else{
		    	// Send a message to the author of the tutorial to ask permission
		    	String target = mTaskManager.getAuthorId();
		    	String tutorialName = mTaskManager.getCurrentTaskInfo().getTaskFolder().getName();
		    	GCMHelper.sendMessage(GCMHelper.TUTORIAL_STARTED, target, GCMHelper.TUTORIAL_NAME, tutorialName, null);
			}
		}
	}

	class CardItemSelectedListener implements AdapterView.OnItemSelectedListener {
		
		int currentPosition = -1;
		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
			// Set current step as the index in mTaskManager.
			Log.d(TAG, "Selected card position: " + position + " and id: " + id);
			
			if(currentPosition!=position){
				Log.d("Read In Task", "Selected card position: " + position + " and id: " + id);
				Log.d("CardScrollView", "Select card:"+position);
				mTaskManager.setStep(position);
				
				populateStepCard(view);
				currentPosition = position;
				
				// Play the audio when you come to a step
				if(mTaskManager.getCurrentAudioFile()!=null){
					new PlayAudio(mTaskManager.getCurrentAudioFile().getAbsolutePath()).onPlay(true);
				}
				
				// Cancel any running timer
				if(cdTimer != null){
					Log.d(TAG, "Canceled a previously running timer before starting a new one");
					cdTimer.cancel();
					cdTimer = null;
				}
				// Start the timer
				if(mTaskManager.getMode() == TaskManager.VIEW_MODE){
					timeSpentInStep = 0;
					cdTimer = new MyCountDownTimer(startTime, interval);
					cdTimer.start();
					Log.d(TAG, "Started the timer in a new step");
				}
			}			
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
			Log.d(TAG, "Current card is displayed.");
		}	
	}
	
	public class updateCardTask extends AsyncTask<File, Void, Bitmap>{
		private View mCardView = null;
		public updateCardTask(View cardView){
			mCardView = cardView;
		}
		
		@Override
		protected void onPostExecute(Bitmap result) {
			super.onPostExecute(result);
			Log.d("Read In Task", "PostExecute");
			if(result!=null){
				ImageView snapView = (ImageView) mCardView.findViewById(R.id.stepcard_snapImg);
				snapView.setImageBitmap(result);
			}
			//mCardScrollView.getAdapter().notifyDataSetChanged();
			
		}

		@Override
		protected Bitmap doInBackground(File... params) {
			Log.d("Read In Task", "doInBackground");
			if(mTaskManager.getCurrentImageFile()!=null){
				Log.d("Read In Task", "UpdateCardImage");
				return decodeBitmap(mTaskManager.getCurrentImageFile());
			}
			return null;
		}		
	}

	@Override
	protected void onResume() {
		super.onResume();
		// Cancel any running timer
		if(cdTimer != null){
			Log.d(TAG, "Canceled a previously running timer before starting a new one in onResume");
			cdTimer.cancel();
			cdTimer = null;
		}
		// Start the timer
		if(mTaskManager.getMode() == TaskManager.VIEW_MODE){
			timeSpentInStep = 0;
			cdTimer = new MyCountDownTimer(startTime, interval);
			cdTimer.start();
			Log.d(TAG, "Started the timer in a new step in onResume");
		}
		
		// Register the broadcast receivers
		if(mAudioChatReceiver == null){
			mAudioChatReceiver = new AudioChatReceiver();
		}
		registerReceiver(mAudioChatReceiver, new IntentFilter(GCMHelper.VIDEO_CHAT));
		
		if(mMessageReceiver == null){
			mMessageReceiver = new MessageReceiver();
		}
		registerReceiver(mMessageReceiver, new IntentFilter(GCMHelper.NOT_NOW));
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_DPAD_CENTER){
			openOptionsMenu();
			return true;
		}
		if(keyCode == KeyEvent.KEYCODE_BACK){
			
			finish();
		}
		return false;
	}
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {

		int id = item.getItemId();
		if (featureId == WindowUtils.FEATURE_VOICE_COMMANDS) {
			Log.d("VoiceCommand", "Using Voice Command");
		}
		
		if(item.getTitle().equals("Stop chat")){
			// End the audio call
			if(mAudioCallUtil != null)
				mAudioCallUtil.endAudioCall(this);
			stopAudioFlag = false;
			return true;
		}
		
		switch(id){
		
		case R.id.action_record:
			// Record a step
			audioWithPicture = true;
			startActivityForResult(new Intent(this, AudioRecordActivity.class),REQUEST_RECORD_AUDIO);
			break;
			
		case R.id.action_next:
			// Go to the next step
			processNextStep();
			break;
			
		case R.id.action_finish:
			// Finish the tutorial
			Toast.makeText(this, "You have completed this tutorial", Toast.LENGTH_SHORT).show();
			if(mTaskManager.getMode()==mTaskManager.EDIT_MODE){
				if(mInstructionManager.isFirstTimeEdit()){
					mInstructionManager.play("You have completed editing this tutorial for the first time");
					mInstructionManager.finishFirstTimeEdit();
				} else {
					mInstructionManager.play("You have completed this tutorial");
					
				}
				
			}
			else if(mTaskManager.getMode()==mTaskManager.VIEW_MODE){
				if(mInstructionManager.isFirstTimeView()){
					mInstructionManager.play("You have completed viewing this tutorial for the first time");
					mInstructionManager.finishFirstTimeView();
				} else {
					mInstructionManager.play("You have completed this tutorial");
				}
				
			}
			
			finish();
//			if(mTaskManager.getMode() == TaskManager.EDIT_MODE){
//				startActivity(new Intent(this, FinishActivity.class));
//			}
			break;
			
		case R.id.action_repeat:
		case R.id.preview_step:
			// In both repeat and preview options, just replay the audio
			if(mTaskManager.getCurrentAudioFile()!=null){
				new PlayAudio(mTaskManager.getCurrentAudioFile().getAbsolutePath()).onPlay(true);
			}
			break;
			
		case R.id.retake_picture:
			// Take a picture
	    	dispatchTakePictureIntent();
			break;
			
		case R.id.rerecord_audio:
			// Record audio
			audioWithPicture = false;
			startActivityForResult(new Intent(this, AudioRecordActivity.class),REQUEST_RECORD_AUDIO);
			break;
			
		case R.id.action_help:
			// Check if the user is same as the author
			if(mTaskManager.getAuthorId().equals(GCMHelper.getRegId())){
				// The tutorial is created by the user itself
				Toast.makeText(this, "You created this tutorial", Toast.LENGTH_SHORT).show(); 	
				mInstructionManager.play("You created this tutorial");
			}
			else if(helpSetting){
				// Start speech recognition to input the message
				boolean result = stt.startSpeechRecognition();
				if(result){
					Log.d(TAG, "Speech recognition started");
				}
				else{
					Log.d(TAG, "Speech recognition not started!");
				}
			}
			else{
				// The author of the tutorial is not available to help at this moment
				Toast.makeText(this, "Author of the tutorial is not available to help now", Toast.LENGTH_SHORT).show(); 	
				mInstructionManager.play("Author of the tutorial is not available to help now");
			}
			break;
		}
		//getWindow().invalidatePanelMenu(WindowUtils.FEATURE_VOICE_COMMANDS);
		invalidateOptionsMenu();
		return super.onMenuItemSelected(featureId, item);
	}	

	@Override
	public boolean onCreatePanelMenu(int featureId, Menu menu) {		
        // Pass through to super to setup touch menu.
        return true;
	}
	
	@Override
	public boolean onPreparePanel(int featureId, View view, Menu menu) {
		Log.d(TAG,"onCreatePanelMenu: "+featureId);
		if(featureId==WindowUtils.FEATURE_VOICE_COMMANDS){
			Log.d("VoiceMenu Test", "onCreatePanelMenu");
			populateMenu(menu);
			return true;
		}
		return super.onPreparePanel(featureId, view, menu);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		populateMenu(menu);
		return true;
	}
	
	public void populateMenu(Menu menu){
		menu.clear();
		switch (state){
		case TaskManager.EDIT_MODE:
			Log.d("VoiceMenu Test","populateMenu on Edit");
			getMenuInflater().inflate(R.menu.task_edit, menu);
			boolean hasAudioFile = (mTaskManager.getCurrentAudioFile()!=null);
			boolean hasImageFile = (mTaskManager.getCurrentImageFile()!=null);
			menu.findItem(R.id.rerecord_audio).setVisible(hasAudioFile);
			menu.findItem(R.id.retake_picture).setVisible(hasImageFile);
			menu.findItem(R.id.preview_step).setVisible(hasAudioFile);
			menu.findItem(R.id.action_next).setVisible(hasAudioFile||hasImageFile);
			menu.findItem(R.id.action_finish).setVisible(hasAudioFile||hasImageFile);
			
			break;
			
		case TaskManager.VIEW_MODE:
			Log.d("VoiceMenu Test","populateMenu on View");
			getMenuInflater().inflate(R.menu.task_view, menu);
			// Add stop audio option
			if(stopAudioFlag){
				menu.add("Stop chat");
			}
			
			break;
		}	

	}
	
	// Method to capture a photo
	private void dispatchTakePictureIntent() {
		Log.d(TAG, "Inside capture image method");
		
	    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
	    	Log.d(TAG, "Capturing the image...");
	        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
	    }
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// Process the captured image
	    if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
	        
	    	String thumbnailPath = data.getStringExtra(Intents.EXTRA_THUMBNAIL_FILE_PATH);
	        //String picturePath = data.getStringExtra(Intents.EXTRA_PICTURE_FILE_PATH);
	    	Log.d(TAG, "Thumbnail path: " + thumbnailPath);
	    	Log.d("CardScrollView","Get Image Result:"+thumbnailPath);
	    	// Copy the image to the current task directory
	    	processImage(thumbnailPath);
	    } 
	    
	    // Process recorded audio
	    if(requestCode == REQUEST_RECORD_AUDIO && resultCode == RESULT_OK){
	    	
	    	String audioPath = data.getStringExtra("audioFilePath");
	    	Log.d("CardScrollView","Get Audio Result:"+audioPath);
	    	mTaskManager.setAudioToCurrentStep(new File(audioPath));
	    	renderAudioPart(mCardScrollView.getSelectedView());
	    	
	    	// Capture the image automatically at the end of audio recording
	    	if(audioWithPicture)
	    		dispatchTakePictureIntent();
	    	else{
	    		// Encourage the user to move on
	    		Toast.makeText(this, "Step complete", Toast.LENGTH_SHORT).show();
	    		mInstructionManager.play("Step complete");
	    	}
	    }
	    
	    // Process the speech to text output
	    if(requestCode == SpeechToText.REQUEST_CODE && resultCode == RESULT_OK){
	    	String message = stt.getSpeechToTextResult(data);
	    	if(message == null){
	    		Log.d(TAG, "Speech not recognized!");
	    		message = "Help me please...";	// Default message
	    	}
	    	else{
	    		Log.d(TAG, "Speech recognized: " + message);
	    	}
	    	
	    	// Get the unique device id
	    	final String deviceID = BluetoothAdapter.getDefaultAdapter().getAddress();
	    	
	    	// Send to the other glass
	    	String target = mTaskManager.getAuthorId();
	    	GCMHelper.sendMessage(GCMHelper.QUICK_HELP, target, GCMHelper.USER_DEVICE_ID, deviceID, message);
	    	
	    	// Show a message to the user asking to wait a while
	    	Toast.makeText(this, "Trying to connect to the helper...", Toast.LENGTH_SHORT).show(); 	
			mInstructionManager.play("Trying to connect to the helper");
	    }
	    super.onActivityResult(requestCode, resultCode, data);
	}
	
	private void processImage(String thumbnailPath){
		File imageFile = new File(thumbnailPath);
		
		// Set image to the current step
		Log.d("MetaInfo", "Set image to current step:"+imageFile.getName());
		mTaskManager.setImageToCurrentStep(imageFile);
		Log.d("MetaInfo", "Get current Image "+mTaskManager.getCurrentImageFile().getName());
		Log.d("CardScrollView", "Add new Image!");
		new updateCardTask(mCardScrollView.getSelectedView()).execute(mTaskManager.getCurrentImageFile());
		
		// Encourage the user to move on
		Toast.makeText(this, "Step complete", Toast.LENGTH_SHORT).show();
		mInstructionManager.play("Step complete");
	}
	
	public Bitmap decodeBitmap(File newFile){
		Log.d("CardScrollView", "Decode Bitmap");
		BitmapFactory.Options options = new BitmapFactory.Options();
    	options.inPreferredConfig = Bitmap.Config.ARGB_8888;
    	Bitmap thumbnail = BitmapFactory.decodeFile(newFile.getAbsolutePath(), options);
    	return thumbnail;
	}
	
	private void processNextStep(){		
		// Create the next step
		mTaskManager.createNextStep();
				
		// Bring the next view to the front
		int posNextView = mTaskManager.getCurrentStep() + 1;
		
		// This is true when user says next step at the last step in the View_Mode
		if(posNextView >= mTaskManager.getTotalSteps()){
			Log.d("Next Step Exception","Finish tutorial");
			if(mTaskManager.getMode()==TaskManager.VIEW_MODE){
				Toast.makeText(this, "You have completed this tutorial", Toast.LENGTH_SHORT).show();
				mInstructionManager.play("You have completed this tutorial");
				finish();
			}
			return ;
		} else {
			//Go to next step.
			Log.d("Next Step Exception","Go to next step");
			mCardScrollView.setSelection(posNextView);
						
			// Set current step in the task manager
			mTaskManager.setStep(posNextView);
		}
	}
	
	@Override
	protected void onPause(){
		if(cdTimer != null){
			cdTimer.cancel();
			cdTimer = null;
			Log.d(TAG, "Canceled cdTimer in onPause");
		}
		
		// Unregister the broadcast receivers
		if(mAudioChatReceiver != null)
			unregisterReceiver(mAudioChatReceiver);
		if(mMessageReceiver != null)
			unregisterReceiver(mMessageReceiver);
		
		// End the audio call
		if(mAudioCallUtil != null)
			mAudioCallUtil.endAudioCall(this);
		stopAudioFlag = false;
		super.onPause();
	}
	
	@Override
	protected void onStop() {

		super.onStop();
		mTaskManager.saveTaskInfo();
		if(cdTimer != null){
			cdTimer.cancel();
			cdTimer = null;
			Log.d(TAG, "Canceled cdTimer in onStop");
		}
		if(mInstructionManager.isFirstTimeEdit()){
			mInstructionManager.finishFirstTimeEdit();
		}
		if(mInstructionManager.isFirstTimeView()){
			mInstructionManager.finishFirstTimeView();
		}
	}

	class StepCardScrollAdapter extends CardScrollAdapter{
		
		@Override
		public int getCount() {
			
			return mTaskManager.getTotalSteps();
			//return mCards.size();	
		}

		@Override
		public Object getItem(int arg0) {
			
			return mCustomizedStepCardView;
			//return mCards.get(arg0);
		}

		@Override
		public int getPosition(Object arg0) {
			
			return mTaskManager.getCurrentStep();
			//return mCards.indexOf(arg0);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Log.d("CardScrollView", "GetView() position:"+position);
			mCustomizedStepCardView = convertView;

			if(convertView == null){
				LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				
				convertView = inflater.inflate(R.layout.stepcard, parent, false);
			}
			
			return convertView;
			//return mCards.get(position).getView(convertView, parent);
		}	
	}
	
	void populateStepCard(View convertView){
		TextView stepText = (TextView) convertView.findViewById(R.id.stepcard_stepnumberText);
		stepText.setText("Step "+(mTaskManager.getCurrentStep()+1));
		renderAudioPart(convertView);
		renderImgPart(convertView);
	}
	
	void renderAudioPart(View convertView){
		Log.d("CardScrollView", "Render Audio");
		ImageView microphoneImg = (ImageView) convertView.findViewById(R.id.stepcard_microphoneImg);
		TextView microphoneText = (TextView) convertView.findViewById(R.id.stepcard_microphoneText);
		if((mTaskManager.getCurrentAudioFile()==null) && (mTaskManager.getMode()==TaskManager.EDIT_MODE)){
			Log.d("CardScrollView", "mTaskManager.getCurrentAudioFile()==null");
			microphoneImg.setImageResource(R.drawable.microphone);
			microphoneImg.setVisibility(View.VISIBLE);
			microphoneText.setVisibility(View.INVISIBLE);
		} 
		else if((mTaskManager.getCurrentAudioFile()==null) && (mTaskManager.getMode()==TaskManager.VIEW_MODE)){
			microphoneImg.setImageResource(R.drawable.noaudio);
			microphoneImg.setVisibility(View.VISIBLE);
			microphoneText.setVisibility(View.INVISIBLE);
		}
		else {
			Log.d("CardScrollView", "mTaskManager.getCurrentAudioFile()!=null");
			microphoneImg.setImageResource(R.drawable.play_icon);
			microphoneImg.setVisibility(View.VISIBLE);
			microphoneText.setVisibility(View.INVISIBLE);
		}
	}
	void renderImgPart(View convertView){
		Log.d("CardScrollView", "Render Bitmap");
		ImageView snapshotImg = (ImageView) convertView.findViewById(R.id.stepcard_snapImg);
		if(mTaskManager.getCurrentImageFile()==null){
			Log.d("CardScrollView", "mTaskManager.getCurrentImageFile==null");
			snapshotImg.setImageResource(R.drawable.camera);
		} else {
			Log.d("CardScrollView", "mTaskManager.getCurrentImageFile!=null");
			new updateCardTask(convertView).execute();
		}
	}
	
	// CountDown Timer Class
	class MyCountDownTimer extends CountDownTimer{
		public MyCountDownTimer(long startTime, long interval){
		   super(startTime, interval);
		}
		   
		@Override
		public void onFinish(){
			// When timer runs out, send a message to the helper
			timeSpentInStep++;
			//mInstructionManager.play("You have spent " + timeSpentInStep + " minutes in this step");
			
			// Check if the user is same as the author
			if(mTaskManager.getAuthorId().equals(GCMHelper.getRegId())){
				// The tutorial is created by the user itself. Do nothing
			}
			else if(helpSetting){
				// Send an update to the helper
				String target = mTaskManager.getAuthorId();
		    	GCMHelper.sendMessage(GCMHelper.TIME_UPDATE, target, GCMHelper.TIME_SOFAR, timeSpentInStep+"", null);
		    	Log.d(TAG, "Sent message to the helper: " + timeSpentInStep + " minutes");
			}
			else{
				// The author of the tutorial is not available to help at this moment
				// Do nothing
			}
			
			// Restart the timer
			// Cancel any running timer
			if(cdTimer != null){
				Log.d(TAG, "Canceled a previously running timer before starting a new one");
				cdTimer.cancel();
				cdTimer = null;
			}
			// Start the timer
			if(mTaskManager.getMode() == TaskManager.VIEW_MODE){
				cdTimer = new MyCountDownTimer(startTime, interval);
				cdTimer.start();
				Log.d(TAG, "Restarted the timer in the same step");
			}
		}
		   
	   @Override
	   public void onTick(long millisUntilFinished){
		   // In each tick of the timer
	   }
	}
	AudioCallUtil mAudioCallUtil;
	
	private class AudioChatReceiver extends BroadcastReceiver {
	    @Override
	    public void onReceive(Context context, Intent intent) {
	        if ((intent.getAction().equals(GCMHelper.VIDEO_CHAT))) {
	        	// Get chat session id
	        	String chatSessionID = intent.getStringExtra(GCMHelper.VIDEO_CHAT_SESSION_ID);
	        	// Start the audio chat
	 		   	mAudioCallUtil = AudioCallUtil.getInstance();
	 		   	mAudioCallUtil.startAudioCall(thisActivity, chatSessionID);
	 		   	stopAudioFlag = true;
	        }
	    }
	}
	
	private class MessageReceiver extends BroadcastReceiver {
	    @Override
	    public void onReceive(Context context, Intent intent) {
	        if ((intent.getAction().equals(GCMHelper.NOT_NOW))) {
	        	// Helper not available now
	        	Toast.makeText(thisActivity, "Helper busy. Please try after sometime...", Toast.LENGTH_SHORT).show(); 	
				InstructionManager.getInstance().play("Helper busy. Please try after sometime.");
	        }
	    }
	}
}
