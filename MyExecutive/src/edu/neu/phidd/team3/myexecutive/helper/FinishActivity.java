package edu.neu.phidd.team3.myexecutive.helper;

import java.io.File;
import com.google.android.glass.view.WindowUtils;
import com.google.android.glass.widget.CardBuilder;
import edu.neu.phidd.team3.myexecutive.R;
import edu.neu.phidd.team3.myexecutive.speech.SpeechToText;
import edu.neu.phidd.team3.myexecutive.task.TaskManager;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

public class FinishActivity extends Activity {
	
	private static final String TAG = FinishActivity.class.getSimpleName();
	TaskManager mTaskManager;
	CardBuilder mainCard;
	SpeechToText stt;
	
	File currentTaskFolder;
	File captionImage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		getWindow().requestFeature(WindowUtils.FEATURE_VOICE_COMMANDS);
		mTaskManager = TaskManager.getInstance();
		stt = new SpeechToText(this);
		
		currentTaskFolder = mTaskManager.getCurrentTaskInfo().getTaskFolder();
		captionImage = mTaskManager.getCurrentTaskInfo().getImageFileByStep(0);
		Bitmap captionImageBitmap = decodeBitmap(captionImage);
		
		mainCard = new CardBuilder(this, CardBuilder.Layout.CAPTION)
					.setText("How To...")
					.addImage(captionImageBitmap);
		setContentView(mainCard.getView());
		Toast.makeText(this, "Almost done. Name the tutorial and save.", Toast.LENGTH_SHORT).show();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.clear();
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.finish, menu);
		return true;
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		if(keyCode == KeyEvent.KEYCODE_DPAD_CENTER){
			openOptionsMenu();
			return true;
		}
		if(keyCode == KeyEvent.KEYCODE_BACK){
			
			finish();
		}
		return false;
	}
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		
		Log.d(TAG, "Id of the selected menu item: " + id);
		if (featureId == WindowUtils.FEATURE_VOICE_COMMANDS) {
			Log.d(TAG, "Using Voice Command");
		}
		
		if(id == R.id.action_record_name){
			
			// Start voice recognition
			boolean result = stt.startSpeechRecognition();
			if(result){
				Log.d(TAG, "Speech recognition started");
			}
			else{
				Log.d(TAG, "Speech recognition not started!");
			}
		}
		else if(id == R.id.action_exit){
			// Exit and go to the main screen
			finish();
		}	
		return super.onMenuItemSelected(featureId, item);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    
	    // Process the speech to text output
	    if(requestCode == SpeechToText.REQUEST_CODE && resultCode == RESULT_OK){
	    	String name = stt.getSpeechToTextResult(data);
	    	if(name == null){
	    		Log.d(TAG, "Speech not recognized! Don't change the name of the tutorial");
	    	}
	    	else{
	    		Log.d(TAG, "Name of the tutorial: " + name);
	    		
	    		// Rename the task folder name
	    		String newName = Environment.getExternalStorageDirectory()
	    				.getPath() + "/MyExecutive/" + name;
	    		File newNameFile = new File(newName);
	    		currentTaskFolder.renameTo(newNameFile);
	    		
	    		// Set the name in this screen
	    		mainCard.setText("How To..." + name);
	    		setContentView(mainCard.getView());
	    	}
	    }
	    super.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	public boolean onCreatePanelMenu(int featureId, Menu menu) {
		return true;
	}

	
	@Override
	public boolean onPreparePanel(int featureId, View view, Menu menu) {
		menu.clear();
        if (featureId == WindowUtils.FEATURE_VOICE_COMMANDS) {
        	// Inflate the menu; this adds items to the action bar if it is present.
    		getMenuInflater().inflate(R.menu.finish, menu);
    		return true;
        }
        // Pass through to super to setup touch menu.
		return super.onPreparePanel(featureId, view, menu);
	}
	
	public Bitmap decodeBitmap(File newFile){
		Log.d("CardScrollView", "Decode Bitmap");
		BitmapFactory.Options options = new BitmapFactory.Options();
    	options.inPreferredConfig = Bitmap.Config.ARGB_8888;
    	Bitmap thumbnail = BitmapFactory.decodeFile(newFile.getAbsolutePath(), options);
    	return thumbnail;
	}
}
