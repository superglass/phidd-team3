package edu.neu.phidd.team3.myexecutive.helper;

import java.io.File;
import java.util.List;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.WindowManager;
import android.os.Environment;
import android.widget.Toast;
import com.google.android.glass.view.WindowUtils;
import com.google.android.glass.widget.CardBuilder;
import edu.neu.phidd.team3.myexecutive.R;
import edu.neu.phidd.team3.myexecutive.audio.InstructionManager;
import edu.neu.phidd.team3.myexecutive.bluetooth.BluetoothService;
import edu.neu.phidd.team3.myexecutive.bluetooth.SendTutorialActivity;
import edu.neu.phidd.team3.myexecutive.gcm.GCMHelper;
import edu.neu.phidd.team3.myexecutive.task.TaskManager;
import edu.neu.phidd.team3.myexecutive.speech.SpeechToText;

public class HelperActivity extends Activity{
	
	private static final String TAG = HelperActivity.class.getSimpleName();
	TaskManager mTaskManager;
	InstructionManager mInstructionManager;
	CardBuilder mainCard;
	BluetoothAdapter mBluetoothAdapter;
	BluetoothService mBluetoothService;
	SpeechToText stt;
	boolean renameMode = false;
	File currentTaskFolder;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		getWindow().requestFeature(WindowUtils.FEATURE_VOICE_COMMANDS);
		mTaskManager = TaskManager.getInstance();
		mInstructionManager = InstructionManager.getInstance();
		stt = new SpeechToText(this);
		
		mainCard = new CardBuilder(this, CardBuilder.Layout.TITLE)
					.addImage(R.drawable.logo_blue);
		setContentView(mainCard.getView());
		
		//setContentView(R.layout.howto_main);
		
		GCMHelper.onCreate(this);
		InstructionManager.Init(getApplicationContext());
		
		//Initializing bluetooth service

		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if(mBluetoothAdapter.isEnabled()){
			if(mBluetoothService == null){
				mBluetoothService = BluetoothService.getInstance();
			}
		}
		//mInstructionManager.resetFirstTimeEdit();
		//mInstructionManager.resetFirstTimeView();
		
		
		
		
	}
	
	private Handler mHandler = new Handler(){

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			switch(msg.what){
			case BluetoothService.MSG_RECEIVED_TUTORIAL:
				invalidateOptionsMenu();
				break;
			}
			super.handleMessage(msg);
		}
		
	};
	
	@Override
	protected void onResume() {
		
		super.onResume();
		invalidateOptionsMenu();
		mBluetoothService.setHandler(mHandler);
		
		mBluetoothService.start();
		if(mInstructionManager.isFirstTimeEdit()&&mInstructionManager.isFirstTimeView()){
			Toast.makeText(this, "Say ok glass to see menu options",Toast.LENGTH_SHORT).show();
			//mInstructionManager.play("Say ok glass to see menu options");
		}
		
	}
	

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.clear();
		Log.d("DeleteFolder", "onPrepareOptionsMenu");
		populateSubMenu(menu);
		return true;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		if(keyCode == KeyEvent.KEYCODE_DPAD_CENTER){
			openOptionsMenu();
			return true;
		}
		if(keyCode == KeyEvent.KEYCODE_BACK){
			
			finish();
		}
		return false;
	}
	
	@Override
	protected void onStop() {
		super.onStop();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		mBluetoothService.stop();
		Log.d("BS tag", "HelperActivity");
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		
		Log.d(TAG, "Id of the selected menu item: " + id);
		if (featureId == WindowUtils.FEATURE_VOICE_COMMANDS) {
			Log.d(TAG, "Using Voice Command");
		}
		if (id == CREATE_TUTORIAL_ID) {
			// Ask the user to name the tutorial
			Toast.makeText(this, "Name the tutorial", Toast.LENGTH_SHORT).show(); 	
			mInstructionManager.play("Name the tutorial");
			// Start speech recognition to input the message 	
			renameMode = false; 	
			boolean result = stt.startSpeechRecognition(); 	
			if(result){
				Log.d(TAG, "Speech recognition started"); 	
			}
			else{
				Log.d(TAG, "Speech recognition not started!"); 	
			}
			
		}  else{
			// If any specific task is selected
			Log.d(TAG, "Menu Title:"+item.getTitle());
			Log.d(TAG, "Menu Id:"+item.getItemId());
			if(item.getItemId()==TaskManager.VIEW_MODE){
				mTaskManager.setMode(TaskManager.VIEW_MODE);
				startActivity(new Intent(this,TaskActivity.class));
			} else if(item.getItemId()==TaskManager.EDIT_MODE){
				mTaskManager.setMode(TaskManager.EDIT_MODE);
				startActivity(new Intent(this,TaskActivity.class));
			} else if(item.getItemId()==TaskManager.SEND_MODE){
				mTaskManager.setMode(TaskManager.SEND_MODE);
				startActivity(new Intent(this,SendTutorialActivity.class));
			} else if(item.getItemId()==TaskManager.DELETE_MODE){
				mTaskManager.deleteTask();
				invalidateOptionsMenu();
				Toast.makeText(this, "Tutorial deleted", Toast.LENGTH_SHORT).show();
				mInstructionManager.play("Tutorial deleted");
			} else if(item.getItemId()==TaskManager.RENAME_MODE){	
				// Give an option to rename a tutorial in case  	
				// the user didn't get the name correct in the first place 	
				renameMode = true; 	
				boolean result = stt.startSpeechRecognition(); 	
				if(result){ 	
					Log.d(TAG, "Speech recognition started"); 	
				}
				else{ 	
					Log.d(TAG, "Speech recognition not started!"); 	
				}
			}
			else {
				Log.d(TAG, "Reading the task info with id: " + id);
				mTaskManager.readTaskInfo(id);
			}
		}		
		return super.onMenuItemSelected(featureId, item);
	}	

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// Process the speech to text output to name a new tutorial
	    if(requestCode == SpeechToText.REQUEST_CODE && resultCode == RESULT_OK && !renameMode){
	    	String name = stt.getSpeechToTextResult(data);
	    	if(name == null){
	    		Log.d(TAG, "Speech not recognized!");
	    		//Name the task with a default name. When user finishes making
				//the tutorial then rename it.
				int n = (int) (Math.random() * 100);
				mTaskManager.createTaskInfo("default" + n);
	    	}
	    	else{
	    		Log.d(TAG, "Speech recognized: " + name);
	    		// Create a task info with the given name
		    	mTaskManager.createTaskInfo(name);
	    	}
	    	
	    	// Start the tutorial in Edit mode
	    	mTaskManager.setMode(TaskManager.EDIT_MODE);
	    	startActivity(new Intent(this, TaskActivity.class));
	    }
	    
	    // Process the speech to text output to rename an existing tutorial
	    if(requestCode == SpeechToText.REQUEST_CODE && resultCode == RESULT_OK && renameMode){
	    	String name = stt.getSpeechToTextResult(data);
	    	if(name == null){
	    		Log.d(TAG, "Speech not recognized!");
	    		// Don't do anything. Keep the previous name
	    	}
	    	else{
	    		Log.d(TAG, "Speech recognized: " + name);
	    		// Rename the task folder name
	    		String newName = Environment.getExternalStorageDirectory()
	    				.getPath() + "/MyExecutive/" + name;
	    		File newNameFile = new File(newName);
	    		currentTaskFolder = mTaskManager.getCurrentTaskInfo().getTaskFolder();
	    		currentTaskFolder.renameTo(newNameFile);
	    	}
	    	Toast.makeText(this, "Tutorial renamed", Toast.LENGTH_SHORT).show();
			mInstructionManager.play("Tutorial renamed");
	    }
	    
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	public boolean onPreparePanel(int featureId, View view, Menu menu) {
		Log.d("DeleteFolder", "onPreparePanelMenu");
        if (featureId == WindowUtils.FEATURE_VOICE_COMMANDS) {
        	Log.d("DeleteFolder", "Feature voice commands");
        	populateSubMenu(menu);
        	
    		return true;
        }
        // Pass through to super to setup touch menu.
		return super.onPreparePanel(featureId, view, menu);
	}

	@Override
	public boolean onCreatePanelMenu(int featureId, Menu menu) {
		return true;
	}
	
	private static final int CREATE_TUTORIAL_ID = -1;
	

	

	public void populateSubMenu(Menu menu){
		Log.d("DeleteFolder", "populateSubMenu");
		menu.clear();

		menu.addSubMenu(Menu.NONE, CREATE_TUTORIAL_ID, Menu.NONE, "Create a tutorial");
	
		
		List<File> tasksFiles = mTaskManager.getAllTaskFolders();
		for(int i = 0; i < tasksFiles.size(); i++){
			// Add submenu to all the tasks so that by using groupId/itemId
			// and name we can get the information of the task name, and which submenu
			// is clicked ("send", "watch", or "edit").
			SubMenu subMenu = menu.addSubMenu(Menu.NONE, i, Menu.NONE, tasksFiles.get(i).getName());
			
			// Add the sub menu entries for each task
			subMenu.add(Menu.NONE, TaskManager.VIEW_MODE, Menu.NONE, "View");
			subMenu.add(Menu.NONE, TaskManager.SEND_MODE, Menu.NONE, "Send");
        	subMenu.add(Menu.NONE, TaskManager.EDIT_MODE, Menu.NONE, "Edit");
        	subMenu.add(Menu.NONE, TaskManager.RENAME_MODE, Menu.NONE, "Rename");
    		subMenu.add(Menu.NONE, TaskManager.DELETE_MODE, Menu.NONE, "Delete");	
		}
	}
}
