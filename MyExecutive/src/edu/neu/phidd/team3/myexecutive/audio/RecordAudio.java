package edu.neu.phidd.team3.myexecutive.audio;

import java.io.IOException;

import android.media.MediaRecorder;
import android.util.Log;

public class RecordAudio{
	
	private static final String TAG = RecordAudio.class.getSimpleName();
	
	// Audio file name with full path
	private String audioFileName;
	
	private MediaRecorder mRecorder = null;
	
	private static RecordAudio instance;
	
	private RecordAudio(){
		mRecorder = new MediaRecorder();
	}
	
	public static RecordAudio getInstance(){
		if(instance==null || instance.mRecorder==null){
			instance = new RecordAudio();
		}
		return instance;
	}
	
	public void onRecord(boolean start){
		if(start){
			startRecording();
		}
		else{
			stopRecording();
		}
	}

	/**
	 * This method set the file where the audio file is to save. 
	 * This must be called before {@link #prepare()};
	 * 
	 * @param fileName
	 */
	public void setAudioFileName(String fileName){
		Log.d("Recording", "Set filepath:"+fileName);
		audioFileName = fileName;
	}
	
	/**
	 * This method will prepare the medioRecorder.
	 * This must be called before {@link #startRecording()}
	 */
	public void prepare(){
		Log.d("Recording", "Prepare Recording");
        mRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mRecorder.setOutputFile(audioFileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

        try {
            mRecorder.prepare();
            Log.d("Recording", "Prepare Success");
        } catch (IOException e) {
            Log.e(TAG, "MediaRecorder prepare() failed!");
        }

	}
	
	/**
	 * This starts recording. This must be called after {@link #prepare()}
	 */
	public void startRecording() {
		Log.d("Recording", "Start Recording");
        mRecorder.start();
    }
	
	/**
	 * This stops recording. This must be called after {@link #startRecording()}
	 */
	public void stopRecording() {
		Log.d("Recording", "Stop Recording");
        mRecorder.stop();
        mRecorder.reset();
        
    }
	
	/**
	 * Release the MediaRecorder.
	 * After this is called, {@link #getInstance()} must be called again to initialize everything.
	 */
	public void releaseRecorder(){
		if (mRecorder != null) {
            mRecorder.release();
            mRecorder = null;
        }
	}
	
	public int getMaxAmplitude(){
		return mRecorder.getMaxAmplitude();
	}
}