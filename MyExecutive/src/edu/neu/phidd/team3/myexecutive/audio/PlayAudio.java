package edu.neu.phidd.team3.myexecutive.audio;

import java.io.IOException;

import android.media.MediaPlayer;
import android.util.Log;

public class PlayAudio{
	
	private static final String TAG = PlayAudio.class.getSimpleName();
	
	// Audio file name with full path
	private String audioFileName;
	
	private MediaPlayer mPlayer = null;
	
	public PlayAudio(String audioFileName){
		this.audioFileName = audioFileName;
	}
	
	public void onPlay(boolean start){
		if(start){
			startPlaying();
		}
		else{
			stopPlaying();
		}
	}
	
	private void startPlaying() {
        mPlayer = new MediaPlayer();
        try {
            mPlayer.setDataSource(audioFileName);
            mPlayer.prepare();
            mPlayer.start();
        } catch (IOException e) {
            Log.e(TAG, "MediaPlayer prepare() failed!");
        }
    }
	
	public void stopPlaying() {
		mPlayer.stop();
        mPlayer.release();
        mPlayer = null;
    }
	
	public void releasePlayer(){
		if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
        }
	}
}