package edu.neu.phidd.team3.myexecutive.audio;

import java.util.Locale;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.speech.tts.TextToSpeech;
import android.util.Log;

public class InstructionManager {
	private static InstructionManager instance;
	private static TextToSpeech tts;
	private boolean usingTTS = true;
	private static Context context = null;
	public static void Init(Context context){
		InstructionManager.context = context;
		tts = new TextToSpeech(context, new TextToSpeech.OnInitListener() {
			
			@Override
			public void onInit(int status) {
				Log.e("TTS","Init TTS");
				if(status == TextToSpeech.SUCCESS){
					int result = tts.setLanguage(Locale.US);
					if(result==TextToSpeech.LANG_MISSING_DATA||result==TextToSpeech.LANG_NOT_SUPPORTED){
						Log.e("TTS","Language Not Supported");
					} 
				}
				
			}
		});
	}
	
	public static InstructionManager getInstance(){
		if(instance==null){
			instance = new InstructionManager();
		}
		return instance;
	}
	
	public void playTestInstruction(){
		play("This is a test");
	}
	
	public  boolean isFirstTimeEdit(){
		SharedPreferences sp = context.getSharedPreferences("howto",context.MODE_PRIVATE);
		return sp.getBoolean("firsttime_edit", true);
	}
	
	public  void finishFirstTimeEdit(){
		SharedPreferences sp = context.getSharedPreferences("howto",context.MODE_PRIVATE);
		Editor e = sp.edit();
		e.putBoolean("firsttime_edit", false);
		e.commit();
	}
	
	public  void resetFirstTimeEdit(){
		SharedPreferences sp = context.getSharedPreferences("howto",context.MODE_PRIVATE);
		Editor e = sp.edit();
		e.putBoolean("firsttime_edit", true);
		e.commit();
	}
	
	
	public  boolean isFirstTimeView(){
		SharedPreferences sp = context.getSharedPreferences("howto",context.MODE_PRIVATE);
		return sp.getBoolean("firsttime_view", true);
	}
	
	public  void finishFirstTimeView(){
		SharedPreferences sp = context.getSharedPreferences("howto",context.MODE_PRIVATE);
		Editor e = sp.edit();
		e.putBoolean("firsttime_view", false);
		e.commit();
	}
	
	public  void resetFirstTimeView(){
		SharedPreferences sp = context.getSharedPreferences("howto",context.MODE_PRIVATE);
		Editor e = sp.edit();
		e.putBoolean("firsttime_view", true);
		e.commit();
	}
	
	public void play(String text){
		if(usingTTS){
			Log.d("TTS", "Speak:"+text);
			tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
		}
	}
	
	public void playRecordInstruction(){
		
	}
	
	public void playTakePicture(){
		
	}
	
	public void playFinishRecording(){
		
	}
	
}
