# README #
##How To...##

This is the class project for the course Personal Health Interface Design and Development using Google Glass. We are Team 3 and our project is HowTo...

Team 3 members:

* Anju Sreevalsan
* Erika Siegel
* Huaqi Yi
* Janice Zhang

### What is this repository for? ###

* Quick summary

"How To" could teach anyone a complex task, by breaking it down into basic steps. The 'teacher' would create step by step tutorials with images and instructions on how to perform a task. The 'learner' would view the tutorial and perform the task. The app also has a live interaction component where the 'teacher' could monitor the 'learner' and help if needed, via a video call. 'How To' could help people with learning disabilities or executive function disorder, to function normally with the help of a teacher.

* Video documentaty on the idea progression
[Team 3_Documentary video + ppt](https://drive.google.com/folderview?id=0B2JmlyHNtpJeY2M2QkxPZWtWYkE&usp=sharing)

* Version 1

### How do I get set up? ###

####Summary of project setup####

1. Set up the [Android SDK](http://developer.android.com/sdk/index.html).

2. Download the [Glass Development Kit Preview](https://developers.google.com/glass/develop/gdk/quick-start). If you are using Eclipse, then download it from Android SDK Manager.

3. Set up the [Google Play Service](https://developer.android.com/google/play-services/setup.html).

4. Use git or SourceTree to checkout the folder 'MyExecutive' from here. Import as Android project. Compile and run.


####Configuration####

When the code is pulled from the repository, sometimes there will be compilation error in AndroidManifest.xml in the line "android:value="@integer/google_play_services_version" ".

This is because the path of google play service library in project.properties is wrong. 

Go to step 3 in the summary of set up to get it right.

If you're using eclipse, it can be fixed by following the steps below:

1. Right click on the project

2. Select Properties

3. Select the Android on the left side.

4. Add the correct reference of google play service and remove the wrong one.


####Wifi connection####

Wifi connection and bluetooth connection are needed to run the app. 

To set up wifi connection for google glass, [check this link](http://www.glassappsource.com/google-glass-how-to/connect-enterprise-wpa-network-google-glass.html).

####Bluetooth setup####

To send tutorial from one glass to the other, the two glass devices must be paired using bluetooth first.

It's tricky to pair two glasses because the setting only allows glass to pair with iphone or android phone, and the intent that makes device discoverable doesn't work on glass.

Below is the trick to pair two glasses.

1. Download and install [Manage Bluetooth](http://ddrboxman.github.io/Bluetooth-Manager-for-Glass/) in both glasses.

2. For one glass, go to setting -> bluetooth -> Android. A video will be played on glass. Don't do any thing and keep it playing. Now this glass will be in discoverable mode.

3. For the other glass, open the Manage Bluetooth which is installed in step 1, tap to open the menu and select Pair New Device. Wait for a few seconds and the first glass will show up in the list.

4. Then you can pair the two glasses.


####Dependencies####

1. Android development tools

2. Glass development kit preview (the name might change in Android SDK Manager)

3. Google Play Service

4. GCM libraries (included in the libs folder)

5. [Open Clove Developer](https://github.com/OpenCloveDeveloper/engageChat) (included in libs folder)

###Instructions to run HowTo###

####Workflow 1: Create a new tutorial####

1. Say "ok glass" in the main glass screen and say "How To".
2. You will come to the main screen of the app.
3. Say "ok glass" and say "Create a tutorial".
4. You will be asked to name the tutorial. Say the name of the new tutorial and it will be processed by the speech recognition API. Now, you will be taken to the first step of the tutorial: Step 1, which is blank at this point.
5. Say "ok glass" and say "Record step".
6. The audio recording will start. You could record the instructions and it will stop automatically once you are done. After the audio recording is complete, the camera will start automatically and capture a picture. You will be asked to accept it. Tap to accept.
7. You will come back to Step 1, and now the image you captured is set as the background of the tutorial. You will also see a play icon instead of the mic icon before, meaning you have recorded the audio as well.
8. This completes the step and you will get an audio message saying "step complete".
9. You could now add another step. Say "ok glass" and from the menu and say "next step". This will add another blank step and you could record it again as explained before.
10. Repeat this and add as many steps to the tutorial as you want. 
11. At any step, you could use the "preview step" option to hear the audio that was recorded for the step.
11. At any step, if you are not satisfied with audio or image of any step, you could retake either from the menu using the "re-take audio" and "re-take picture" options.
12. Once you are done adding steps to the tutorial, select the "finish tutorial" option. This will bring you back to the main screen. 
13. After finishing the tutorial, say "ok glass" from the main screen and you will get the main menu, where the tutorial you just created will be listed.

####Workflow 2: View an existing tutorial####

1. Say "ok glass" in the main glass screen and say "How To".
2. You will come to the main screen of the app.
3. Say "ok glass" and say the name of any tutorial listed in the menu.
4. You will be shown a submenu, giving different options on what to do with the tutorial, and here say "View".
5. The tutorial opens and shows you the first step in it with the image pre-recorded for that step. It will also play the instruction recorded for this step automatically.
6. You could replay the instructions any number of times in a step by going to the menu and selecting "Replay" option. Everything is completely handsfree in the View mode, so that the user of the tutorial could perform the task while watching the tutorial.
7. You can navigate through the different steps by saying "next step" in the menu. 
8. Once you have reached the end of the tutorial, from the last step, when you say "next step" it will exit the tutorial and you will be shown a message that "you have completed this tutorial". It will also be played back to you as an audio using text to speech. 
9. You can also exit the tutorial at any step by selecting the "finish tutorial" option from the menu.

####Workflow 3: Send the tutorial to another glass####

1. From the main screen of the app, say "ok glass" to see the main menu. Select the tutorial you want to send.
2. This will open a submenu asking what to do with the tutorial. Say "send".
3. This will show a message "tap to see devices". The two glasses should be paired via bluetooth beforehand using the instructions we have given in the "READ ME" section of bit bucket. The two glasses should be in bluetooth range and paired beforehand to be able to transfer the tutorial. 
4. Also, the glass that wants to receive the tutorial should have the app running in the forefront at the moment. 
4. In the screen, "tap to see devices", tap and you will see all the paired devices in the menu. Select the glass you want to send the tutorial to.
5. This will create a bluetooth connection and the tutorial will be transferred to the other glass.
6. The receiver glass will get a notification as an audio cue that the tutorial has been received.
7. At the receiver glass, go to the main menu now, and you can see the transferred tutorial listed in the list of tutorials.


####Workflow 4: Live communication between the helper and the user####

1. The person who created the tutorial is designated as the helper and his glass id is saved with the tutorial. This enables us to send messages to the helper from any user glass device.
2. When a user opens the tutorial in 'View' mode, the helper of the tutorial is notified in the background, unbeknownst to the user, via google cloud messaging. The message sent to the helper says " the user has started the tutorial <name of the tutorial>, do you want to help?". 
3. The helper can choose 'yes' or 'no'. 
4. If yes, that means he will get help requests from the user and also updates on the time spent by the user in each step during this particular tutorial viewing session.  
5. If he selected no, then he won't get any updates or help request from the helper for this particular session. If the user requests help, he will be shown a message that the helper is not available at this moment.
6. Now, at the user end, every tutorial step has a "Seek help" menu option. If the user needs help with a step, he could select this option, and this would prompt him to say a short message. This message is detected by speech recognition and is sent to the helper via google cloud messaging.
7. When the helper device gets the message, it pops up a live card on his device which shows the message the user just sent. 
8. In this live card, the helper has two options: "Help" and "Dismiss".
9. If he selects "Help", an audio chat is initiated between the helper and the user and both would join the call, where they could talk to each other and resolve the confusion.
10. If he selects "Dismiss", that means he doesn't want to start the audio chat. So, the user will get a message that the helper is not available at this moment.

####Workflow 5: Passive monitoring by the helper####

1. In addition to explicit "seek help" option, the helper can passively monitor the  user and intervene if appropriate.
2. For this, we have implemented a timer in the view mode of the tutorial. If the user of the tutorial is taking too much time in a particular step (1 minute in our case, could be modified easily), then the helper will get a notification. He will keep getting a notification every minute the user is in the same step. 
3. The helper can now select the 'Help' option from the live card at any moment to start the audio chat as described before. 

### Who do I talk to? ###

* Anju Sreevalsan
* Huaqi Yi
* Stephen Intille